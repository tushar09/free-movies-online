package triumphit.free.movie.online;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FlashActivity extends AppCompatActivity {

    String maintenanceMode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        hit();
    }

    void hit() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/maintenance.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("maintenance");
                            JSONObject mode = jArr.getJSONObject(0);
                            maintenanceMode = mode.getString("maintenancemode");
                            if(maintenanceMode.equals("yes")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                                builder.setTitle("FREE MOVIE ONLINE")
                                        .setMessage("Sorry, server under maintenance. Please wait for 1 hour.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        })
                                        .setIcon(R.drawable.main_x);
                                final AlertDialog a = builder.create();
                                a.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                                    }
                                });
                                a.show();
                                Log.e("maintenance", maintenanceMode + " from run");
                            }else if(maintenanceMode.equals("no")){
                                Intent i = new Intent(FlashActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                            Log.e("maintenance", maintenanceMode);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("FREE MOVIE ONLINE")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.drawable.main_x);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}


