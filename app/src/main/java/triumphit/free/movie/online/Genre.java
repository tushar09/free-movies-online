package triumphit.free.movie.online;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ironsource.mobilcore.AdUnitEventListener;
import com.ironsource.mobilcore.MobileCore;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import triumphit.free.movie.online.adapter.LVAdapter;

import static android.R.attr.id;

public class Genre extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ListView lv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout mLinearScroll;
    int totalNumberOfMovie = 0;
    String onRefresh = null;
    Button previousClicked;
    String page = "page 1";
    private AnalyticsTrackers at;
    private Tracker t;
    private AdView av;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    String search = "";
    int s = 0, e = 29;
    private ViewGroup v;
    private Snackbar snackbar;
    private RevMobFullscreen fullscreen;
    private RevMob revmob;
    private StartAppAd startAppAd = new StartAppAd(this);

    void showFullScreenAdRevmob() {
        //Fullscreen with listener:
        fullscreen.show();
        //MobileCore.showInterstitial(this, MobileCore.AD_UNIT_TRIGGER.APP_START, null);
    }

    private void showAd() {
        if (sp.getString("ad", "null").equals("revmob")) {
            showFullScreenAdRevmob();
        } else if(sp.getString("ad", "null").equals("mobilecore")){
            //Toast.makeText(MainActivity.this, "mobilecore", Toast.LENGTH_SHORT).show();
            MobileCore.setAdUnitEventListener(new AdUnitEventListener() {
                @Override
                public void onAdUnitEvent(MobileCore.AD_UNITS adUnit, EVENT_TYPE eventType,
                                          MobileCore.AD_UNIT_TRIGGER... trigger) {
                    if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_INIT_SUCCEEDED) {
                        MobileCore.loadAdUnit(MobileCore.AD_UNITS.INTERSTITIAL,
                                MobileCore.AD_UNIT_TRIGGER.MAIN_MENU);
                        //Toast.makeText(MainActivity.this, "first if", Toast.LENGTH_SHORT).show();
                    }
                    else if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_READY) {
                        for(MobileCore.AD_UNIT_TRIGGER myTrigger:trigger){
                            if(myTrigger.equals(MobileCore.AD_UNIT_TRIGGER.MAIN_MENU)){
                                MobileCore.showInterstitial(Genre.this,
                                        MobileCore.AD_UNIT_TRIGGER.MAIN_MENU, null);
                                //Toast.makeText(MainActivity.this, "sec if", Toast.LENGTH_SHORT).show();
                            }else{
                                //Toast.makeText(MainActivity.this, "else", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }else if(sp.getString("ad", "null").equals("startapp")){
            startAppAd.showAd(); // show the ad
            startAppAd.loadAd(); // load the next ad
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //showAd();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        v = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);
            }
        });

        fullscreen = revmob.createFullscreen(Genre.this, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                Log.e("RevMob", "Fullscreen loaded.");

            }

            @Override
            public void onRevMobAdNotReceived(String message) {
                Log.e("RevMob", "Fullscreen not received.");
            }

            @Override
            public void onRevMobAdDismissed() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen Dismissed")
                        .setAction("Revmob Full Dismissed " + sp.getString("account", "notset"))
                        .setLabel("Clicked Dismissed " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdClicked() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen")
                        .setAction("Revmob Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdDisplayed() {
                Log.e("RevMob", "Fullscreen displayed.");
            }
        });

        search = getIntent().getStringExtra("search");

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        av = (AdView) findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        av.loadAd(adRequest);
        av.setAdListener(new AdListener() {
            @Override
            public void onAdOpened() {
                super.onAdOpened();
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Ad")
                        .setAction("ad opened Genre: " + getSupportActionBar().getTitle().toString() + sp.getString("account", "null"))
                        .setLabel("Banner opened " + getSupportActionBar().getTitle().toString() + sp.getString("account", "null"))
                        .build());
            }
        });

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();


        lv = (ListView) findViewById(R.id.lv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        //new PullMovieInfo().execute(getIntent().getStringExtra("playLink"));
                                        search(s, e, search);
                                    }
                                }
        );

    }

    @Override
    public void onRefresh() {
        if (onRefresh == null) {
            //new PullMovieInfo().execute(getIntent().getStringExtra("playLink"));
            search(s, e, search);
        } else {
            search(s, e, search);
        }
    }

    private class PullMovieInfo extends AsyncTask<String, String, String> {

        ArrayList id, name, size, rate, year, resume, genre, srt, quality, download, image, descp;
        private ArrayList view;
        private ArrayList downloaded;

        @Override
        protected String doInBackground(String... params) {
            id = new ArrayList();
            name = new ArrayList();
            size = new ArrayList();
            rate = new ArrayList();
            year = new ArrayList();
            resume = new ArrayList();
            genre = new ArrayList();
            srt = new ArrayList();
            quality = new ArrayList();
            download = new ArrayList();
            image = new ArrayList();
            descp = new ArrayList();
            view = new ArrayList();
            downloaded = new ArrayList();

            try {
                JSONObject jsonObject1 = new JSONObject(params[0]);
                JSONArray jArr = jsonObject1.getJSONArray("FullMovie");
                JSONArray SearchResultsArr = jsonObject1.getJSONArray("SearchResults");
                //JSONObject searchResult = jsonObject1.getJSONObject("SearchResults");
                JSONObject total = SearchResultsArr.getJSONObject(0);
                totalNumberOfMovie = Integer.parseInt("" + total.get("total"));


                for (int t = 0; t < jArr.length(); t++) {
                    JSONObject jsonObject = jArr.getJSONObject(t);
                    id.add(jsonObject.get("id"));
                    name.add(jsonObject.get("name"));
                    size.add(jsonObject.get("size"));
                    rate.add(jsonObject.get("rating"));
                    year.add(jsonObject.get("year"));
                    resume.add(jsonObject.get("resume_capability"));
                    genre.add(jsonObject.get("genre"));
                    srt.add(jsonObject.get("srt"));
                    quality.add(jsonObject.get("quality"));
                    download.add(jsonObject.get("download"));
                    image.add(jsonObject.get("thumbnail"));
                    descp.add(jsonObject.get("description"));
                    downloaded.add(jsonObject.get("downloaded"));
                    view.add(jsonObject.get("view"));
                }
            } catch (JSONException e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LVAdapter adapter = new LVAdapter(Genre.this, id, name, size, rate, year, resume, genre, srt, download, quality, image, descp, view, downloaded);
            //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
            lv.setAdapter(adapter);
            int button = totalNumberOfMovie / 30;
            if (totalNumberOfMovie % 30 != 0) {
                button++;
            }
            button = button - mLinearScroll.getChildCount();
            setReadyPagination(button);
            swipeRefreshLayout.setRefreshing(false);
            getSupportActionBar().setTitle(getIntent().getStringExtra("genre") + ": " + page);
        }

        private void setReadyPagination(int button) {
            if (name != null && name.size() != 0) {
                //int size = (totalNumberOfMovie / 30) + 1;

                for (int j = 0; j < button; j++) {
                    final int k;
                    k = j;
                    final Button btnPage = new Button(Genre.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 0);
                    btnPage.setTextColor(Color.WHITE);
                    btnPage.setTextSize(13.0f);
                    btnPage.setId(j);
                    btnPage.setText(String.valueOf(mLinearScroll.getChildCount() + 1));
                    btnPage.setBackgroundResource(R.drawable.button_unfocused);
                    if (j == 0) {
                        previousClicked = btnPage;
                        btnPage.setBackgroundResource(R.drawable.button_focus);
                        previousClicked.setBackgroundResource(R.drawable.button_focus);
                        btnPage.setTextColor(Color.parseColor("#000000"));
                    }
                    mLinearScroll.addView(btnPage, lp);

                    btnPage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            previousClicked.setBackgroundResource(R.drawable.pagebuttontranssss);
                            previousClicked.setTextColor(Color.parseColor("#ffffff"));

                            btnPage.setBackgroundResource(R.drawable.pagebuttontrans);
                            btnPage.setTextColor(Color.parseColor("#000000"));
                            TransitionDrawable transition = (TransitionDrawable) btnPage.getBackground();
                            transition.startTransition(250);
                            TransitionDrawable transition2 = (TransitionDrawable) previousClicked.getBackground();
                            transition2.startTransition(250);
                            previousClicked = btnPage;
                            onRefresh = getIntent().getStringExtra("playLink");
                            String s = "&i=" + (30 * Integer.parseInt("" + btnPage.getId())) + "&f=" + ((30 * Integer.parseInt("" + (btnPage.getId() + 1))) - 1);
                            onRefresh = onRefresh.replace("&i=0&f=29", s);
                            page = "page " + (btnPage.getId() + 1);
                            //onRefresh = "http://burakcikcik.com/?user=admin&pass=b9bd0091200b1ebb77ee33fb58ef9cd1&i=" + (30 * Integer.parseInt("" + btnPage.getId())) + "&f=" + ((30 * Integer.parseInt("" + (btnPage.getId() + 1))) - 1);
                            new PullMovieInfo().execute(onRefresh);

                        }
                    });
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    void search(final int s, final int e, String asdfasd) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/genre.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(snackbar != null){
                                    snackbar.dismiss();
                                }
                            }
                        });
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", "usertest");
                params.put("email", "asdf1234");
                params.put("password", "asdf1234");
                String key = printHashKey(Genre.this);
                String creds = String.format("%s", key);
                String auth = Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                params.put("keyhash", auth);
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("genre", search);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public String printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "triumphit.free.movie.online",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        return "null";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
