package triumphit.free.movie.online;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ironsource.mobilcore.AdUnitEventListener;
import com.ironsource.mobilcore.MobileCore;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.banner.RevMobBanner;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;

import java.util.HashMap;
import java.util.Map;

public class DirectContact extends AppCompatActivity {

    SeekBar value;
    ImageView hate, like, love;
    EditText name, email, mess;
    Snackbar snackbar;
    private RevMobFullscreen fullscreen;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private AnalyticsTrackers at;
    private Tracker t;
    private RevMob revmob;
    private StartAppAd startAppAd = new StartAppAd(this);


    void showFullScreenAdRevmob() {
        //Fullscreen with listener:
        fullscreen.show();
        //MobileCore.showInterstitial(this, MobileCore.AD_UNIT_TRIGGER.APP_START, null);
    }

    private void showAd() {
        if (sp.getString("ad", "null").equals("revmob")) {
            showFullScreenAdRevmob();
        } else if(sp.getString("ad", "null").equals("mobilecore")){
            //Toast.makeText(MainActivity.this, "mobilecore", Toast.LENGTH_SHORT).show();
            MobileCore.setAdUnitEventListener(new AdUnitEventListener() {
                @Override
                public void onAdUnitEvent(MobileCore.AD_UNITS adUnit, EVENT_TYPE eventType,
                                          MobileCore.AD_UNIT_TRIGGER... trigger) {
                    if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_INIT_SUCCEEDED) {
                        MobileCore.loadAdUnit(MobileCore.AD_UNITS.INTERSTITIAL,
                                MobileCore.AD_UNIT_TRIGGER.MAIN_MENU);
                        //Toast.makeText(MainActivity.this, "first if", Toast.LENGTH_SHORT).show();
                    }
                    else if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_READY) {
                        for(MobileCore.AD_UNIT_TRIGGER myTrigger:trigger){
                            if(myTrigger.equals(MobileCore.AD_UNIT_TRIGGER.MAIN_MENU)){
                                MobileCore.showInterstitial(DirectContact.this,
                                        MobileCore.AD_UNIT_TRIGGER.MAIN_MENU, null);
                                //Toast.makeText(MainActivity.this, "sec if", Toast.LENGTH_SHORT).show();
                            }else{
                                //Toast.makeText(MainActivity.this, "else", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }else if(sp.getString("ad", "null").equals("startapp")){
            startAppAd.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        //showAd();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_contact);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);
            }
        });

        fullscreen = revmob.createFullscreen(DirectContact.this, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                Log.e("RevMob", "Fullscreen loaded.");

            }

            @Override
            public void onRevMobAdNotReceived(String message) {
                Log.e("RevMob", "Fullscreen not received.");
            }

            @Override
            public void onRevMobAdDismissed() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen Dismissed")
                        .setAction("Revmob Full Dismissed " + sp.getString("account", "notset"))
                        .setLabel("Clicked Dismissed " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdClicked() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen")
                        .setAction("Revmob Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdDisplayed() {
                Log.e("RevMob", "Fullscreen displayed.");
            }
        });

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        hate = (ImageView) findViewById(R.id.imageView12);
        like = (ImageView) findViewById(R.id.imageView13);
        love = (ImageView) findViewById(R.id.imageView14);

        name = (EditText) findViewById(R.id.input_name);
        email = (EditText) findViewById(R.id.input_email);
        mess = (EditText) findViewById(R.id.input_message);

        hate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setProgress(0);
            }
        });
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setProgress(1);
            }
        });
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setProgress(2);
            }
        });

        value = (SeekBar) findViewById(R.id.seekBar2);
        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                if(isValidEmail(email.getText())){
                    if(!name.getText().toString().equals("")){
                        if(!mess.getText().toString().equals("")){
                            if(value.getProgress() == 0){
                                sendMessage(name.getText().toString(), email.getText().toString(), mess.getText().toString() + ": Hated it", view);
                            }else if(value.getProgress() == 2){
                                sendMessage(name.getText().toString(), email.getText().toString(), mess.getText().toString() + ": Loved it", view);
                            }else{
                                sendMessage(name.getText().toString(), email.getText().toString(), mess.getText().toString() + ": Liked it", view);
                            }

                        }else{
                            snackbar = Snackbar
                                    .make(view, "Your message is empty", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("OK", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            snackbar.dismiss();
                                        }
                                    });

                            snackbar.show();
                        }
                    }else{
                        snackbar = Snackbar
                                .make(view, "Name field is empty", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        snackbar.dismiss();
                                    }
                                });

                        snackbar.show();
                    }
                }else{
                    snackbar = Snackbar
                            .make(view, "Invalid email address", Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    snackbar.dismiss();
                                }
                            });

                    snackbar.show();
                }
            }
        });
    }

    private void sendMessage(final String name, final String email, final String mess, final View view) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/directContact.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("before report", response);
                        //Toast.makeText(context, response.replaceAll("\"", ""), Toast.LENGTH_SHORT).show();
                        snackbar = Snackbar
                                .make(view, response.replaceAll("\"", ""), Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        snackbar.dismiss();
                                    }
                                });

                        snackbar.show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackbar = Snackbar
                                .make(view, "Could not connect to the internet", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        snackbar.dismiss();
                                    }
                                });

                        snackbar.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("message", mess);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(DirectContact.this);
        requestQueue.add(stringRequest);

        t.send(new HitBuilders.EventBuilder()
                .setCategory("Direct Contact")
                .setAction(name + ": " + email)
                .setLabel(mess)
                .build());
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


}
