package triumphit.free.movie.online.DownloadManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import triumphit.free.movie.online.MainActivity;
import triumphit.free.movie.online.R;

public class DownloadService extends Service {

    public final IBinder myService = new MyLocalBinder();
    private Context context;

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        context = this;
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        runAsForeground(intent.getStringExtra("title"), intent.getStringExtra("description"), intent.getStringExtra("photo"));
        return START_STICKY;
    }

    private void runAsForeground(String title, String description, String photo) {
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.downloader);
        Intent stopIntent = new Intent(context, Stop.class);
        PendingIntent stop = PendingIntent.getBroadcast(this, 0, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        remoteViews.setTextViewText(R.id.textView49, title);
        remoteViews.setOnClickPendingIntent(R.id.imageView16, stop);
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.about_icon)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, "Close", stop)
                .build();
        startForeground(1, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myService;
    }

    public class MyLocalBinder extends Binder {
        public DownloadService getService(){
            return DownloadService.this;
        }
    }

    public String getString(){
        return "asdfsdaf";
    }

    public static class Stop extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("asdf", "stop");
            context.stopService(new Intent(context, DownloadService.class));
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(1);
        }
    }

    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
