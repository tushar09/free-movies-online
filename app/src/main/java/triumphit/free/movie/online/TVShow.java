package triumphit.free.movie.online;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.splash.SplashConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import triumphit.free.movie.online.adapter.TVShowAdapter;

public class TVShow extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;
    ListView lv;
    Snackbar snackbar;

    String test = "http://triumphit.tech/tvShow.php";
    ArrayList name, totalSeason, rating, year, genre, image;
    private int totalNumberOfMovie;
    LinearLayout mLinearScroll;
    private Button previousClicked;

    int s = 0;
    int e = 29;
    private MenuItem actionbarMenu;
    String onRefresh = null;
    Toolbar toolbar;
    private String sort = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvshow);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        StartAppAd.showSplash(this, savedInstanceState, new SplashConfig()
                .setTheme(SplashConfig.Theme.USER_DEFINED)
                .setCustomScreen(R.layout.content_flash));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        lv = (ListView) findViewById(R.id.listView);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        hit(s, e, sort);
                                    }
                                }
        );
    }

    private void hit(final int s, final int e, final String sort) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, test,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response tv show", response);
                        name = new ArrayList();
                        totalSeason = new ArrayList();
                        year = new ArrayList();
                        genre = new ArrayList();
                        image = new ArrayList();
                        rating = new ArrayList();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jArr = jsonObject.getJSONArray("FullShow");

                            JSONArray totalItems = jsonObject.getJSONArray("Total");
                            JSONObject total = totalItems.getJSONObject(0);
                            totalNumberOfMovie = total.getInt("total");

                            for (int t = 0; t < jArr.length(); t++) {
                                JSONObject jObject = jArr.getJSONObject(t);
                                name.add(jObject.get("name"));
                                year.add(jObject.get("year"));
                                genre.add(jObject.get("genre"));
                                rating.add(jObject.get("rating"));
                                image.add(jObject.get("thumbnail"));
                                totalSeason.add("Seasons: " + jObject.get("totalSeason"));
                            }

                            int button = totalNumberOfMovie / 30;
                            if (totalNumberOfMovie % 30 != 0) {
                                button++;
                            }
                            button = button - mLinearScroll.getChildCount();
                            TVShowAdapter tsa = new TVShowAdapter(TVShow.this, name, totalSeason, rating, year, genre, image);
                            lv.setAdapter(tsa);
                            setReadyPagination(button);
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            Log.e("response tv show", e1.toString());
                            TVShowAdapter tsa = new TVShowAdapter(TVShow.this, name, totalSeason, rating, year, genre, image);
                            lv.setAdapter(tsa);
                        }
                        //new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (snackbar != null) {
//                                    snackbar.dismiss();
//                                }
//                            }
//                        });
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("sort", "" + sort);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onRefresh() {
        hit(s, e, sort);
    }

    public void setReadyPagination(int button) {
        if (name != null && name.size() != 0) {
            //int size = (totalNumberOfMovie / 30) + 1;

            for (int j = 0; j < button; j++) {
                final int k;
                k = j;
                final Button btnPage = new Button(TVShow.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, 0, 0, 0);
                btnPage.setTextColor(Color.WHITE);
                btnPage.setTextSize(13.0f);
                btnPage.setId(j);
                btnPage.setText(String.valueOf(mLinearScroll.getChildCount() + 1));
                btnPage.setBackgroundResource(R.drawable.button_unfocused);
                if (j == 0) {
                    previousClicked = btnPage;
                    btnPage.setBackgroundResource(R.drawable.button_focus);
                    previousClicked.setBackgroundResource(R.drawable.button_focus);
                    btnPage.setTextColor(Color.parseColor("#000000"));
                }
                mLinearScroll.addView(btnPage, lp);


                btnPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        previousClicked.setBackgroundResource(R.drawable.pagebuttontranssss);
                        previousClicked.setTextColor(Color.parseColor("#ffffff"));

                        btnPage.setBackgroundResource(R.drawable.pagebuttontrans);
                        btnPage.setTextColor(Color.parseColor("#000000"));
                        TransitionDrawable transition = (TransitionDrawable) btnPage.getBackground();
                        transition.startTransition(250);
                        TransitionDrawable transition2 = (TransitionDrawable) previousClicked.getBackground();
                        transition2.startTransition(250);
                        previousClicked = btnPage;
                        s = (30 * btnPage.getId());
                        e = ((30 * (btnPage.getId() + 1)) - 1);
                        Log.e("adfdsafadsfasdf", e + "");
                        hit(s, e, sort);
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        getMenuInflater().inflate(R.menu.tvshow_menu, menu);
        actionbarMenu = menu.findItem(R.id.action_settings);
        //actionbarMenu.setVisible(false);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 2000);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);
        MenuItem mi = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        if (onRefresh == null) {
                            hit(s, e, sort);
                        } else {
                            hit(s, e, sort);
                        }
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        hit(0, totalNumberOfMovie, "id");
                        return true; // Return true to expand action view
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Search(newText);
                return true;
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.sort){
            PopupMenu pm = new PopupMenu(TVShow.this, toolbar);
            MenuInflater mi = pm.getMenuInflater();
            mi.inflate(R.menu.tvshow_sort, pm.getMenu());
            pm.show();

            pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if(item.getItemId() == R.id.sortByName){
                        sort = "name";
                        hit(s, e, "name");
                    }
                    if(item.getItemId() == R.id.sortByRating){
                        sort = "rating";
                        hit(s, e, "rating");
                    }
                    if(item.getItemId() == R.id.sortByYear){
                        sort = "year";
                        hit(s, e, "year");
                    }
                    if(item.getItemId() == R.id.sortById){
                        sort = "id";
                        hit(s, e, "id");
                    }
                    return false;
                }
            });

        }
        return true;
    }

    private void Search(String newText) {
        ArrayList name, totalSeason, rating, year, genre, image;
        name = new ArrayList();
        totalSeason = new ArrayList();
        year = new ArrayList();
        genre = new ArrayList();
        image = new ArrayList();
        rating = new ArrayList();

        if (this.name != null && this.name.size() != 0) {
            for (int t = 0; t < this.name.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.name.get(t)).find()) {
                    name.add(this.name.get(t));
                    totalSeason.add(this.totalSeason.get(t));
                    year.add(this.year.get(t));
                    genre.add(this.genre.get(t));
                    image.add(this.image.get(t));
                    rating.add(this.rating.get(t));
                }
            }
        }

        TVShowAdapter tsa = new TVShowAdapter(TVShow.this, name, totalSeason, rating, year, genre, image);
        lv.setAdapter(tsa);

    }
}
