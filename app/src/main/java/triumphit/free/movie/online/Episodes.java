package triumphit.free.movie.online;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ironsource.mobilcore.AdUnitEventListener;
import com.ironsource.mobilcore.MobileCore;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;

import java.util.ArrayList;

import triumphit.free.movie.online.adapter.EpisodeAdapter;

public class Episodes extends AppCompatActivity {

    ArrayList dowload, title, released, episodes, imdbrating;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private RevMob revmob;
    private RevMobFullscreen fullscreen;
    private AnalyticsTrackers at;
    private Tracker t;
    ListView lv;
    private StartAppAd startAppAd = new StartAppAd(this);
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.listView3);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        getSupportActionBar().setTitle(getIntent().getStringExtra("name") + ": Episodes");

        dowload = getIntent().getStringArrayListExtra("download");
        title = getIntent().getStringArrayListExtra("title");
        released = getIntent().getStringArrayListExtra("released");
        episodes = getIntent().getStringArrayListExtra("episodes");
        imdbrating = getIntent().getStringArrayListExtra("imdbRating");
        for(int t = 0; t < title.size(); t++){
            Log.e("Download", "" + title.get(t));
        }

        EpisodeAdapter ea = new EpisodeAdapter(Episodes.this, dowload, title, released, episodes, imdbrating, getIntent().getStringExtra("name"));
        lv.setAdapter(ea);

        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);
            }
        });

        fullscreen = revmob.createFullscreen(Episodes.this, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                Log.e("RevMob", "Fullscreen loaded.");

            }

            @Override
            public void onRevMobAdNotReceived(String message) {
                Log.e("RevMob", "Fullscreen not received.");
            }

            @Override
            public void onRevMobAdDismissed() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen Dismissed")
                        .setAction("Revmob Full Dismissed " + sp.getString("account", "notset"))
                        .setLabel("Clicked Dismissed " + sp.getString("account", "notset"))
                        .build());

                Bundle params = new Bundle();
                params.putString("Revmob Full Screen Dismissed", sp.getString("account", "notset"));
                mFirebaseAnalytics.logEvent("Revmob Full Screen Dismissed", params);
            }

            @Override
            public void onRevMobAdClicked() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen")
                        .setAction("Revmob Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());

                Bundle params = new Bundle();
                params.putString("Revmob Full Screen Clicked", sp.getString("account", "notset"));
                mFirebaseAnalytics.logEvent("Revmob Full Screen Clicked", params);
            }

            @Override
            public void onRevMobAdDisplayed() {
                Log.e("RevMob", "Fullscreen displayed.");
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //showAd();
    }

    private void showAd() {
        if (sp.getString("ad", "null").equals("revmob")) {
            showFullScreenAdRevmob();

        } else if (sp.getString("ad", "null").equals("mobilecore")) {
            //Toast.makeText(MainActivity.this, "mobilecore", Toast.LENGTH_SHORT).show();
            MobileCore.setAdUnitEventListener(new AdUnitEventListener() {
                @Override
                public void onAdUnitEvent(MobileCore.AD_UNITS adUnit, EVENT_TYPE eventType,
                                          MobileCore.AD_UNIT_TRIGGER... trigger) {
                    if (adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_INIT_SUCCEEDED) {
                        MobileCore.loadAdUnit(MobileCore.AD_UNITS.INTERSTITIAL,
                                MobileCore.AD_UNIT_TRIGGER.MAIN_MENU);
                        //Toast.makeText(MainActivity.this, "first if", Toast.LENGTH_SHORT).show();
                    } else if (adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_READY) {
                        for (MobileCore.AD_UNIT_TRIGGER myTrigger : trigger) {
                            if (myTrigger.equals(MobileCore.AD_UNIT_TRIGGER.MAIN_MENU)) {
                                MobileCore.showInterstitial(Episodes.this,
                                        MobileCore.AD_UNIT_TRIGGER.MAIN_MENU, null);
                                //Toast.makeText(MainActivity.this, "sec if", Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(MainActivity.this, "else", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }else if(sp.getString("ad", "null").equals("startapp")){
            startAppAd.showAd(); // show the ad
            startAppAd.loadAd(); // load the next ad
        }
    }

    void showFullScreenAdRevmob() {
        //Fullscreen with listener:
        fullscreen.show();
        //MobileCore.showInterstitial(this, MobileCore.AD_UNIT_TRIGGER.APP_START, null);
    }

}
