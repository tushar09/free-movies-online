package triumphit.free.movie.online.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.util.ArrayList;

import triumphit.free.movie.online.AnalyticsTrackers;
import triumphit.free.movie.online.Player;
import triumphit.free.movie.online.R;

/**
 * Created by Tushar on 5/19/2016.
 */
public class EpisodeAdapter extends BaseAdapter {

    Context context;
    ArrayList download, title, relesed, episode, imdbRating;
    String name;
    private final AnalyticsTrackers at;
    private final Tracker t;

    LayoutInflater inflater;
    private FirebaseAnalytics mFirebaseAnalytics;

    public EpisodeAdapter(Context context, ArrayList download, ArrayList title, ArrayList relesed, ArrayList episode, ArrayList imdbRating, String name){
        this.context = context;
        this.download = download;
        this.title = title;
        this.episode = episode;
        this.relesed = relesed;
        this.imdbRating = imdbRating;
        this.name = name;

        AnalyticsTrackers.initialize(context);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return title.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = inflater.inflate(R.layout.episode_row, null);

            holder.title = (TextView) convertView.findViewById(R.id.textView51);
            holder.episode = (TextView) convertView.findViewById(R.id.textView52);
            holder.released = (TextView) convertView.findViewById(R.id.textView53);
            holder.imdbRating = (TextView) convertView.findViewById(R.id.textView54);
            holder.download = (ImageButton) convertView.findViewById(R.id.imageButton9);

            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        holder.title.setText("" + title.get(position));
        holder.episode.setText("Episode: " + episode.get(position));
        holder.released.setText("" + relesed.get(position));
        holder.imdbRating.setText("" + imdbRating.get(position) + "/10");

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Player.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("srt", ("" + download.get(position)).substring(0, ("" + download.get(position)).length() - 4) + ".srt")
                        .putExtra("name", "" + title.get(position))
                        .putExtra("playLink", "" + download.get(position)));
                Log.e("srt lin", ("" + download.get(position)).substring(0, ("" + download.get(position)).length() - 4) + ".srt");
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("TVShow Viewed")
                        .setAction("" + title.get(position))
                        .setLabel("Played " + title.get(position))
                        .build());

                Bundle params = new Bundle();
                params.putString("TVShow Viewed", "" + title.get(position));
                mFirebaseAnalytics.logEvent("TVShow Viewed", params);
            }
        });

        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("d link", "" + download.get(position));
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("FREE MOVIES ONLINE")
                        .setMessage("Do you want to download " + title.get(position))
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Bundle params = new Bundle();
                                params.putString("Downloaded", "" + title.get(position));
                                mFirebaseAnalytics.logEvent("Downloaded", params);

                                DownloadManager.Request r = new DownloadManager.Request(Uri.parse("" + download.get(position)));
                                Log.e("download Clicked", download.get(position) + "");
                                String downloadPath = "/Free Movies Online";
                                File fileDir = new File(downloadPath);

                                if (fileDir.isDirectory()) {
                                    String extention = "";
                                    if (("" + download.get(position)).endsWith(".mkv")) {
                                        extention = ".mkv";
                                    } else if (("" + download.get(position)).endsWith(".MKV")) {
                                        extention = ".MKV";
                                    } else if (("" + download.get(position)).endsWith(".MP4")) {
                                        extention = ".MP4";
                                    } else if (("" + download.get(position)).endsWith(".mp4")) {
                                        extention = ".mp4";
                                    } else if (("" + download.get(position)).endsWith(".AVI")) {
                                        extention = ".AVI";
                                    } else if (("" + download.get(position)).endsWith(".avi")) {
                                        extention = ".avi";
                                        Log.e("download Clicked", "avi");
                                    } else if (("" + download.get(position)).endsWith(".3gp")) {
                                        extention = ".3gp";
                                    } else if (("" + download.get(position)).endsWith(".3GP")) {
                                        extention = ".3GP";
                                    }
                                    r.setDestinationInExternalPublicDir(Environment.DIRECTORY_MOVIES, name + " - episode -" + episode.get(position) + " - " + title.get(position) + extention);
                                } else {
                                    fileDir.mkdirs();
                                    String extention = "";
                                    if (("" + download.get(position)).endsWith(".mkv")) {
                                        extention = ".mkv";
                                    } else if (("" + download.get(position)).endsWith(".MKV")) {
                                        extention = ".MKV";
                                    } else if (("" + download.get(position)).endsWith(".MP4")) {
                                        extention = ".MP4";
                                    } else if (("" + download.get(position)).endsWith(".mp4")) {
                                        extention = ".mp4";
                                    } else if (("" + download.get(position)).endsWith(".AVI")) {
                                        extention = ".AVI";
                                    } else if (("" + download.get(position)).endsWith(".avi")) {
                                        extention = ".avi";
                                        Log.e("download Clicked", "avi else");
                                    } else if (("" + download.get(position)).endsWith(".3gp")) {
                                        extention = ".3gp";
                                    } else if (("" + download.get(position)).endsWith(".3GP")) {
                                        extention = ".3GP";
                                    }
                                    r.setDestinationInExternalPublicDir(Environment.DIRECTORY_MOVIES, name + " - episode -" + episode.get(position) + " - " + title.get(position) + extention);
                                }
                                // This put the download in the same Download dir the browser uses
                                //r.setDestinationInExternalPublicDir(dir, "" + name.get(position));

                                // When downloading music and videos they will be listed in the player
                                // (Seems to be available since Honeycomb only)
                                r.allowScanningByMediaScanner();

                                // Notify user when download is completed
                                // (Seems to be available since Honeycomb only)
                                r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                // Start download
                                DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                dm.enqueue(r);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.main_x);
                final AlertDialog a = builder.create();
                a.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    }
                });
                a.show();
                //holder.description.setVisibility(VISIBLE);
            }
        });

        return convertView;
    }

    class Holder{
        TextView title, episode, released, imdbRating;
        ImageButton download;
    }

}
