package triumphit.free.movie.online;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.ChasingDots;
import com.github.ybq.android.spinkit.style.Circle;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.github.ybq.android.spinkit.style.FoldingCube;
import com.github.ybq.android.spinkit.style.Pulse;
import com.github.ybq.android.spinkit.style.RotatingPlane;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.github.ybq.android.spinkit.style.WanderingCubes;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ironsource.mobilcore.AdUnitEventListener;
import com.ironsource.mobilcore.MobileCore;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyVideoAd;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.video.VideoListener;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.VideoView;

public class Player extends AppCompatActivity implements io.vov.vitamio.MediaPlayer.OnTimedTextListener, io.vov.vitamio.MediaPlayer.OnInfoListener {

    InterstitialAd mInterstitialAd;

    private SurfaceHolder holder;

    private static VideoView mPreview;
    private static boolean handlerCheck = false;

    io.vov.vitamio.MediaPlayer.OnTimedTextListener ott;

    ProgressBar loading;


    Animation slide_down;
    Animation slide_up;
    Animation slide_in_left;
    Animation slide_in_right;
    Animation slide_out_left;
    Animation slide_out_right;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Runnable runnable;
    Handler handler;

    static TextView timer, totalTime, sub, charg, subTitleText;
    static ImageButton play, next, pre, lock, sound;
    static SeekBar sb, bright, volume;
    static RelativeLayout rl, base, base2, main;
    static long counter = 0;

    static RelativeLayout controller;

    static boolean locker = false;
    static boolean b = false;

    static ArrayList time;
    static Subtitle subtitle;
    private String accountName;
    private AnalyticsTrackers at;
    private Tracker t;
    private RevMobFullscreen fullscreen;
    private RevMob revmob;
    AdColonyVideoAd ad;
    private StartAppAd startAppAd = new StartAppAd(this);
    private StartAppAd startAppAdVid = new StartAppAd(this);

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //showAd();
    }

    void showFullScreenAdRevmob() {
        //Fullscreen with listener:
        fullscreen.show();
        //MobileCore.showInterstitial(this, MobileCore.AD_UNIT_TRIGGER.APP_START, null);
    }

    private void showAd() {
        if (sp.getString("ad", "null").equals("revmob")) {
            showFullScreenAdRevmob();
        } else if(sp.getString("ad", "null").equals("mobilecore")){
            //Toast.makeText(MainActivity.this, "mobilecore", Toast.LENGTH_SHORT).show();
            MobileCore.setAdUnitEventListener(new AdUnitEventListener() {
                @Override
                public void onAdUnitEvent(MobileCore.AD_UNITS adUnit, EVENT_TYPE eventType,
                                          MobileCore.AD_UNIT_TRIGGER... trigger) {
                    if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_INIT_SUCCEEDED) {
                        MobileCore.loadAdUnit(MobileCore.AD_UNITS.INTERSTITIAL,
                                MobileCore.AD_UNIT_TRIGGER.MAIN_MENU);
                        //Toast.makeText(MainActivity.this, "first if", Toast.LENGTH_SHORT).show();
                    }
                    else if(adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_READY) {
                        for(MobileCore.AD_UNIT_TRIGGER myTrigger:trigger){
                            if(myTrigger.equals(MobileCore.AD_UNIT_TRIGGER.MAIN_MENU)){
                                MobileCore.showInterstitial(Player.this,
                                        MobileCore.AD_UNIT_TRIGGER.MAIN_MENU, null);
                                //Toast.makeText(MainActivity.this, "sec if", Toast.LENGTH_SHORT).show();
                            }else{
                                //Toast.makeText(MainActivity.this, "else", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }else if(sp.getString("ad", "null").equals("startapp")){
            startAppAd.showAd(); // show the ad
            startAppAd.loadAd(); // load the next ad
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Vitamio.isInitialized(this);

        AdColony.configure(Player.this, "version:1.5,store:google", "app35278ef72d4d4877b5", "vz7f2a8b5a3a424192a7");
        ad = new AdColonyVideoAd("vz7f2a8b5a3a424192a7");

        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);
            }
        });
        fullscreen = revmob.createFullscreen(Player.this, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                Log.e("RevMob", "Fullscreen loaded.");

            }

            @Override
            public void onRevMobAdNotReceived(String message) {
                Log.e("RevMob", "Fullscreen not received.");
            }

            @Override
            public void onRevMobAdDismissed() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen Dismissed")
                        .setAction("Revmob Full Dismissed " + sp.getString("account", "notset"))
                        .setLabel("Clicked Dismissed " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdClicked() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen")
                        .setAction("Revmob Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void onRevMobAdDisplayed() {
                Log.e("RevMob", "Fullscreen displayed.");
            }
        });

        mPreview = (io.vov.vitamio.widget.VideoView) findViewById(R.id.vitamio_videoView);
        mPreview.setOnTimedTextListener(this);
        mPreview.setOnInfoListener(this);
        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        ott = this;
        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        loading = (ProgressBar)findViewById(R.id.spin_kit);

        Random r = new Random();
        int tx  = r.nextInt(11);

        if(tx == 0){
            RotatingPlane cd = new RotatingPlane();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 1){
            DoubleBounce cd = new DoubleBounce();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 2){
            Wave cd = new Wave();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 3){
            WanderingCubes cd = new WanderingCubes();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 4){
            Pulse cd = new Pulse();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 5){
            ChasingDots cd = new ChasingDots();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 6){
            ThreeBounce cd = new ThreeBounce();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 7){
            Circle cd = new Circle();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 8){
            CubeGrid cd = new CubeGrid();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 9){
            FadingCircle cd = new FadingCircle();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }else if(tx == 10){
            FoldingCube cd = new FoldingCube();
            cd.setAnimationDelay(2000);
            loading.setIndeterminateDrawable(cd);
        }

        b = true;

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2851562992535827/8948124199");
        requestNewInterstitial();
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Ad")
                        .setAction("ad opened Player " + sp.getString("account", "null"))
                        .setLabel("Interstitial opened " + sp.getString("account", "null"))
                        .build());
            }
        });

        slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        slide_in_left = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_left);
        slide_in_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_right);
        slide_out_left = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_out_left);
        slide_out_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_out_right);

        controller = (RelativeLayout) findViewById(R.id.controller);

        //mPreview = (SurfaceView) findViewById(R.id.main2);

        play = (ImageButton) findViewById(R.id.imageButton);
        pre = (ImageButton) findViewById(R.id.imageButton3);
        next = (ImageButton) findViewById(R.id.imageButton2);
        lock = (ImageButton) findViewById(R.id.imageButton4);
        sound = (ImageButton) findViewById(R.id.imageButton5);
        rl = (RelativeLayout) findViewById(R.id.lockMusk);
        base = (RelativeLayout) findViewById(R.id.base);
        base2 = (RelativeLayout) findViewById(R.id.base2);
        main = (RelativeLayout) findViewById(R.id.main);
        timer = (TextView) findViewById(R.id.textView12);
        totalTime = (TextView) findViewById(R.id.textView13);
        sub = (TextView) findViewById(R.id.textView14);
        subTitleText = (TextView) findViewById(R.id.textView16);
        charg = (TextView) findViewById(R.id.textView15);
        sb = (SeekBar) findViewById(R.id.seekBar);
        volume = (SeekBar) findViewById(R.id.volume);
        bright = (SeekBar) findViewById(R.id.bright);

        Typeface face = Typeface.createFromAsset(getAssets(), "captureit.ttf");
        sub.setTypeface(face);
        charg.setTypeface(face);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controller.getVisibility() == View.GONE) {
                    controller.setVisibility(View.VISIBLE);
                    base.setVisibility(View.VISIBLE);
                    base2.setVisibility(View.VISIBLE);
                    controller.startAnimation(slide_up);
                    base.startAnimation(slide_in_left);
                    base2.startAnimation(slide_in_right);
                    slide_up.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            counter = 0;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    counter = 0;
                }
            }
        });

        rl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return locker;
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long t = mPreview.getCurrentPosition();
                t = t + 15000;
                if (t < mPreview.getDuration()) {
                    mPreview.seekTo(t);
                }

            }
        });

        next.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                long t = mPreview.getCurrentPosition();
                t = t + 15000;
                if (t < mPreview.getDuration()) {
                    mPreview.seekTo(t);
                }
                return false;
            }
        });

        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long t = mPreview.getCurrentPosition();
                t = t - 15000;
                if (t > 0) {
                    mPreview.seekTo(t);
                }
            }
        });

        pre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                long t = mPreview.getCurrentPosition();
                t = t - 15000;
                if (t > 0) {
                    mPreview.seekTo(t);
                }
                return false;
            }
        });

        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locker) {
                    lock.setBackgroundResource(R.drawable.uloc);
                    locker = false;
                } else {
                    lock.setBackgroundResource(R.drawable.loc);
                    locker = true;
                }
            }
        });

        sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPreview != null) {
                    if (sp.getBoolean("mute", true)) {
                        mPreview.setVolume(0, 0);
                        sound.setBackgroundResource(R.drawable.mute);
                        editor.putBoolean("mute", false);
                        editor.commit();
                    } else {
                        mPreview.setVolume(1, 1);
                        sound.setBackgroundResource(R.drawable.unmute);
                        editor.putBoolean("mute", true);
                        editor.commit();
                    }
                }
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPreview != null) {
                    if (mPreview.isPlaying()) {
                        mPreview.pause();
                        play.setBackgroundResource(R.drawable.ply);
                    } else {
                        mPreview.start();
                        play.setBackgroundResource(R.drawable.pus);
                    }
                }
            }
        });

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                update();
                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(runnable, 0);
            handlerCheck = true;
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                if(fromUser){
//                    if(mPreview != null){
//                        mPreview.pause();
//                        mPreview.seekTo(progress);
//                    }
//                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mPreview.pause();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seek(seekBar.getProgress() * 1000);
            }
        });
        volume.setProgress(10);
        volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    counter = 0;
                    if (mPreview != null) {
                        mPreview.setVolume((float) progress / 10.0f, (float) progress / 10.0f);
                        if (progress == 0) {
                            sound.setBackgroundResource(R.drawable.mute);
                            editor.putBoolean("mute", false);
                            editor.commit();
                        } else {
                            sound.setBackgroundResource(R.drawable.unmute);
                            editor.putBoolean("mute", true);
                            editor.commit();
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        try {
            float curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
            bright.setProgress((int) curBrightnessValue);
        } catch (Settings.SettingNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        bright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    counter = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Settings.System.canWrite(Player.this)) {
                            android.provider.Settings.System.putInt(getContentResolver(),
                                    android.provider.Settings.System.SCREEN_BRIGHTNESS, progress);
                        }else {
                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                            intent.setData(Uri.parse("package:" + Player.this.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new SetDownloadSrt().execute(getIntent().getStringExtra("srt"), getIntent().getStringExtra("name"));
                if (subTitleText.getVisibility() == View.VISIBLE) {
                    subTitleText.setVisibility(View.GONE);
                } else {
                    subTitleText.setVisibility(View.VISIBLE);
                }
                counter = 0;
                //mPreview.addTimedTextSource(null);
                //mPreview.setTimedTextShown(true);
            }
        });

        mPreview.setVideoPath(getIntent().getStringExtra("playLink"));
        mPreview.requestFocus();
        mPreview.start();

        new SetDownloadSrt().execute(getIntent().getStringExtra("srt"), getIntent().getStringExtra("name"));

        mPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setPlaybackSpeed(1.0f);
                sb.setMax((int) mPreview.getDuration() / 1000);
                totalTime.setText(timeConversion((int) mPreview.getDuration() / 1000));
            }
        });

    }

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            charg.setText(String.valueOf(level) + "%");
        }
    };

    private void seek(int i) {
        if (mPreview != null) {
            mPreview.seekTo(i);
            mPreview.start();
        }
    }

    private boolean needResume;
    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                //Begin buffer, pause playing
                if (mPreview.isPlaying()) {
                    mPreview.pause();
                    needResume = true;
                }
                loading.setVisibility(View.VISIBLE);
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                //The buffering is done, resume playing
                if (needResume)
                    mPreview.start();
                loading.setVisibility(View.GONE);
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                //Display video download speed
                break;
        }
        return true;
    }


    class SetDownloadSrt extends AsyncTask<String, String, String> {

        String name = "";
        String srt = "";

        @Override
        protected String doInBackground(String... params) {
            name = params[1];
            srt = params[0];

            int index = srt.lastIndexOf('/') + 1;
            srt = srt.substring(index, srt.length());
            Log.e("srt  from do inback", srt);
            int count;
            try {
                URL url = new URL(params[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + File.separator + srt);
                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String srtName = "";
            if(srt.endsWith(".srt")){
                srtName = srt;
            }else{
                srtName = unpackZip(Environment.getExternalStorageDirectory() + File.separator, srt);
            }

            subtitle = new Subtitle(Player.this, Environment.getExternalStorageDirectory() + File.separator + srtName);
            time = null;
            time = subtitle.getTiming();
        }
    }

    String getTimer(long l) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
//        int h = (int) ((time / 1000) / 3600);
//        int m = (int) (((time / 1000) / 60) % 60);
//        int s = (int) ((time / 1000) % 60);
//        int ss = (int) time % 1000;

        //String ret = h + ":" + m + ":" + s + "," + ss;

        final long hr = TimeUnit.MILLISECONDS.toHours(l);
        final long min = TimeUnit.MILLISECONDS.toMinutes(l - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        //final long ms = TimeUnit.MILLISECONDS.toMillis(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(sec));
        //return String.format("%02d:%02d:%02d,%03d", hr, min, sec);
        return String.format("%02d:%02d:%02d", hr, min, sec);

        //return new SimpleDateFormat("hh:mm:ss,SSS").format(new Date(time));
    }


    private String unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        String filename = "";
        try {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;


                filename = ze.getName();
                FileOutputStream fout = new FileOutputStream(path + filename);

                // reading and writing
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zis.closeEntry();
            }
            zis.close();
        } catch (Exception e) {
            return "error";
        }
        return filename;
    }

    int c = 0;
    String temp = "";


    int a = 0;
    private void update() {
        a++;
        //Log.e("testing", "" + a);
        if(a == 1460){
            startAppAdVid.loadAd(StartAppAd.AdMode.REWARDED_VIDEO);
        }
        if(a == 1500){
            a = 0;
            startAppAdVid.show();
            if(mPreview != null){
                mPreview.pause();
            }
            play.setBackgroundResource(R.drawable.ply);
            startAppAdVid.setVideoListener(new VideoListener() {
                @Override
                public void onVideoCompleted() {
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("StartApp Video Complete")
                            .setAction("StartApp Video " + sp.getString("account", "notset"))
                            .setLabel("StartApp Video " + sp.getString("account", "notset"))
                            .build());
                }
            });
            //ad = new AdColonyVideoAd("vz7f2a8b5a3a424192a7");
        }
        if (temp.equals(subTitleText.getText().toString())) {
            c++;
            if (c == 8) {
                subTitleText.setText("");
                c = 0;
            }
        }

        if (mPreview != null) {
            if (time != null && mPreview.isPlaying()) {
                String g = getTimer(mPreview.getCurrentPosition() + 2000);
                for (int t = 0; t < time.size(); t++) {
                    String s = "" + time.get(t);
                    if (s.contains(g)) {
                        String text = subtitle.getDialog(t);
                        text = text.replaceAll("<i>", "");
                        text = text.replaceAll("</i>", "");
                        text = text.substring(text.indexOf('\n') + 1);
                        temp = text;
                        subTitleText.setText(text);
                        //time.remove(t);
                        c = 0;
                        break;
                    } else {

                    }
                }
            }
        }

        if (mPreview != null) {
            sb.setProgress((int) mPreview.getCurrentPosition() / 1000);
            timer.setText(timeConversion((int) mPreview.getCurrentPosition() / 1000));
        }
        counter++;
        if (counter == 8) {
            controller.startAnimation(slide_down);
            base.startAnimation(slide_out_left);
            base2.startAnimation(slide_out_right);
            slide_down.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    controller.setVisibility(View.GONE);
                    base.setVisibility(View.GONE);
                    base2.setVisibility(View.GONE);
                    //counter = 0;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

        }else if(counter > 8){
            if(b == true){
                counter = 0;
                b = false;
            }

        }

    }



    private static String timeConversion(int totalSecs) {

        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

        return timeString;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBatInfoReceiver);
        if (mPreview != null) {
            mPreview.stopPlayback();
            mPreview = null;
        }

        b = true;
        a = 0;
        finish();
    }

    @Override
    public void onTimedText(String text) {
    }

    @Override
    public void onTimedTextUpdate(byte[] pixels, int width, int height) {

    }
}
