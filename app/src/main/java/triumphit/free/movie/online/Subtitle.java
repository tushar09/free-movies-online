package triumphit.free.movie.online;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Tushar on 3/18/2016.
 */
public class Subtitle {

    Context context;
    String srtName;
    ArrayList alist, atime, adia;

    Subtitle(Context context, String srtName){
        this.context = context;
        this.srtName = srtName;
        adia = new ArrayList();
        alist = new ArrayList();
        atime = new ArrayList();
        fetchData();
    }


    String list = "";
    String time = "";
    String dia = "";
    int c = 0;
    private void fetchData() {
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(srtName));
            String line;

            while ((line = br.readLine()) != null) {
                if(line.equals("")){
                    alist.add(list);
                    atime.add(time);
                    adia.add(dia);
                    list = "";
                    time = "";
                    dia = "";
                    c = 0;
                }else{
                    c++;
                    if(c == 1){
                        list = line;
                    }
                    if(c == 2){
                        int startIndex = line.indexOf(",");
                        int endIndex = line.indexOf(">");
                        String replacement = " ";
                        String toBeReplaced = line.substring(startIndex , endIndex + 1);
                        line = line.replace(toBeReplaced, replacement);
                        time = line;
                        time = time.substring(0, time.length()- 4);
                    }else{
                        dia = dia + line + '\n';
                    }

                }
                //text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
        }
        catch (StringIndexOutOfBoundsException siob){

        }

    }

    public ArrayList getTiming(){
        return atime;
    }

    public String getDialog(int index){
//        if(atime.contains(time)){
//            //int remove = atime.indexOf(time);
//            String dia =
//            //atime.remove(remove);
//            //adia.remove(remove);
//            return dia;
//        }else{
//            return "null";
//        }
        return "" + adia.get(index);
    }

    public ArrayList getList(){
        return alist;
    }

}
