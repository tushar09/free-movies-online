package triumphit.free.movie.online;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.startapp.android.publish.StartAppAd;

import triumphit.free.movie.online.adapter.TVShowSeasonsAdapter;

public class Seasons extends AppCompatActivity {

    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seasons);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String seasons = getIntent().getStringExtra("seasons");
        String name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);

        String [] availSeasons = seasons.split("-");

//        if(seasons.contains("-")){
//            availSeasons = seasons.split("-");
//        }else{
//            availSeasons = new String[1];
//            availSeasons[0] = seasons;
//        }

        lv = (ListView) findViewById(R.id.listView2);
        TVShowSeasonsAdapter tssa = new TVShowSeasonsAdapter(this, availSeasons, getIntent().getStringExtra("name"));
        lv.setAdapter(tssa);
    }

}
