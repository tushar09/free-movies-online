package triumphit.free.movie.online;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ironsource.mobilcore.AdUnitEventListener;
import com.ironsource.mobilcore.MobileCore;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyVideoAd;
import com.pushbots.push.Pushbots;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.interstitial.RevMobFullscreen;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.vov.vitamio.Vitamio;
import triumphit.free.movie.online.adapter.LVAdapter;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {



    String test = "http://triumphit.tech/volleyRegister.php";
    String sortLink = "http://triumphit.tech/sort.php";
    private static String link = "";
    int s = 0;
    int e = 29;

    ViewGroup v;


    int pos = 0;
    Menu menu;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    ListView lv;
    private SwipeRefreshLayout swipeRefreshLayout;
    ArrayList id, name, size, rate, year, resume, genre, srt, quality, download, image, descp, views, downloaded;
    private LinearLayout mLinearScroll;

    Tracker t;
    AnalyticsTrackers at;
    FirebaseDatabase fdb;
    DatabaseReference drf;

    int totalNumberOfMovie = 0;
    String onRefresh = null;

    Button previousClicked;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private MenuItem actionbarMenu;
    private String accountName;
    private Snackbar snackbar;
    private RevMob revmob;
    private RevMobFullscreen fullscreen;
    private TextView home, noticeText;
    private TextView noticeMes;
    private StartAppAd startAppAd = new StartAppAd(this);

    private FirebaseAnalytics mFirebaseAnalytics;

    static {
        System.loadLibrary("mkvCodec");
    }

    public native String loadCodec();

    private FloatingActionButton fab;

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            pos = 0;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd");
            String formattedDate = df.format(c.getTime());
            int date = Integer.parseInt(formattedDate);
            if (date % sp.getInt("reviewCycle", 3) == 0) {
                if (sp.getBoolean("review", true)) {
                    editor.putBoolean("review", false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("REVIEW FREE MOVIE ONLINE")
                            .setMessage("Please rate this app. Your review will help us to improve our FREE MOVIE ONLINE.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                        //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
                                        //Intent i = new Intent(Intent.ACTION_VIEW);
                                        //i.setData(Uri.parse(url));
                                        //startActivity(i);
                                    }
                                    editor.putInt("reviewCycle", 30);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            })
                            .setNegativeButton("Never", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    editor.putInt("reviewCycle", 20);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editor.putInt("reviewCycle", 3);// do nothing
                            editor.commit();
                            finish();
                        }
                    })
                            .setIcon(R.drawable.main_x);

                    final AlertDialog a = builder.create();
                    a.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                            a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    });
                    a.show();
                    editor.commit();

                } else {
                    finish();
                    //showAd();
                }
                //moveTaskToBack(false);

                return true;
            } else {
                editor.putBoolean("review", true);
                editor.commit();
                //showAd();
            }
            //onBackPressed();

        }
        return super.onKeyDown(keyCode, event);
    }

    private void showAd() {
        if (sp.getString("ad", "null").equals("revmob")) {
            showFullScreenAdRevmob();
        } else if (sp.getString("ad", "null").equals("mobilecore")) {
            //Toast.makeText(MainActivity.this, "mobilecore", Toast.LENGTH_SHORT).show();
            MobileCore.setAdUnitEventListener(new AdUnitEventListener() {
                @Override
                public void onAdUnitEvent(MobileCore.AD_UNITS adUnit, EVENT_TYPE eventType,
                                          MobileCore.AD_UNIT_TRIGGER... trigger) {
                    if (adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_INIT_SUCCEEDED) {
                        MobileCore.loadAdUnit(MobileCore.AD_UNITS.INTERSTITIAL,
                                MobileCore.AD_UNIT_TRIGGER.MAIN_MENU);
                        //Toast.makeText(MainActivity.this, "first if", Toast.LENGTH_SHORT).show();
                    } else if (adUnit == MobileCore.AD_UNITS.INTERSTITIAL &&
                            eventType == AdUnitEventListener.EVENT_TYPE.AD_UNIT_READY) {
                        for (MobileCore.AD_UNIT_TRIGGER myTrigger : trigger) {
                            if (myTrigger.equals(MobileCore.AD_UNIT_TRIGGER.MAIN_MENU)) {
                                MobileCore.showInterstitial(MainActivity.this,
                                        MobileCore.AD_UNIT_TRIGGER.MAIN_MENU, null);
                                //Toast.makeText(MainActivity.this, "sec if", Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(MainActivity.this, "else", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        } else if (sp.getString("ad", "null").equals("startapp")) {
            startAppAd.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    ViewGroup view;


    void showFullScreenAdRevmob() {
        //Fullscreen with listener:
        fullscreen.show();
        //MobileCore.showInterstitial(this, MobileCore.AD_UNIT_TRIGGER.APP_START, null);
    }

    private void startAd() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/ad.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject1 = null;
                        try {
                            jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("ad");
                            JSONObject jo = jArr.getJSONObject(0);
                            Log.e("asdfasdfasdfsdaf", jo.getString("activeAd"));
                            if (jo.getString("activeAd").equals("revmob")) {
                                revmob = RevMob.startWithListener(MainActivity.this, new RevMobAdsListener() {

                                });
                                editor.putString("ad", "revmob");
                                editor.commit();
                                //showFullScreenAdRevmob();


                                //}else if(jo.getString("activeAd").equals("flurry")){
                                //view.setVisibility(View.GONE);
                            } else if (jo.getString("activeAd").equals("mobilecore")) {
                                editor.putString("ad", "mobilecore");
                                editor.commit();
                            } else if (jo.getString("activeAd").equals("startapp")) {
                                editor.putString("ad", "startapp");
                                editor.commit();
                            }

                        } catch (JSONException e1) {
                            Log.e("ad", e1.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }

    AdColonyVideoAd ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Vitamio.isInitialized(this);
        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().setAlias(accountName + " " + Build.DEVICE + ": " + Build.MODEL);

        StartAppSDK.init(this, "203741702", true);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        fdb = FirebaseDatabase.getInstance();

        drf = fdb.getReference("news");

        drf.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>)dataSnapshot.getValue();
                Log.e("link", map.get("link").toString());
                Log.e("text", map.get("text").toString());
                Log.e("version", map.get("version").toString());
                noticeMes.setText(map.get("text").toString());
                link = map.get("link").toString();

                try {
                    if(getPackageManager().getPackageInfo(getPackageName(), 0).versionName.toString().equals(map.get("version").toString())){
                        fab.setVisibility(View.GONE);
                    }else{
                        fab.setVisibility(View.VISIBLE);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    //about.setText("Version Unknown");
                }

                //noticeMes.setText(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        home = (TextView) toolbar.findViewById(R.id.textView45);
        noticeText = (TextView) toolbar.findViewById(R.id.textView46);
        noticeMes = (TextView) toolbar.findViewById(R.id.textView47);
        noticeMes.setAutoLinkMask(Linkify.WEB_URLS);
        noticeMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("c", "clicked");
                List<String> extractedUrls = extractUrls(noticeMes.getText().toString());
                try{
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);
                }catch (Exception e){

                }
            }
        });
        getSupportActionBar().setTitle("");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("FREE MOVIE ONLINE")
                        .setMessage("Update your app")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://triumphit.tech/movies/"));
                                startActivity(browserIntent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.main_x);
                final AlertDialog a = builder.create();
                a.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                });
                a.show();
            }
        });

        AdColony.configure(MainActivity.this, "version:1.5,store:google", "app35278ef72d4d4877b5", "vz7f2a8b5a3a424192a7");
        ad = new AdColonyVideoAd("vz7f2a8b5a3a424192a7");

        revmob = RevMob.startWithListener(this, new RevMobAdsListener() {
            @Override
            public void onRevMobSessionIsStarted() {
                //revmob.showBanner(MainActivity.this);
            }
        });
        fullscreen = revmob.createFullscreen(MainActivity.this, new RevMobAdsListener() {
            @Override
            public void onRevMobAdReceived() {
                Log.e("RevMob", "Fullscreen loaded.");

            }

            @Override
            public void onRevMobAdNotReceived(String message) {
                Log.e("RevMob", "Fullscreen not received.");
            }

            @Override
            public void onRevMobAdDismissed() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen Dismissed")
                        .setAction("Revmob Full Dismissed " + sp.getString("account", "notset"))
                        .setLabel("Clicked Dismissed " + sp.getString("account", "notset"))
                        .build());
                Bundle params = new Bundle();
                params.putString("Revmob Full Screen Dismissed", sp.getString("account", "notset"));
                params.putString("Revmob Full Dismissed ", sp.getString("account", "notset"));
                mFirebaseAnalytics.logEvent("Revmob Full Screen Ad Dismissed", params);
            }

            @Override
            public void onRevMobAdClicked() {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Revmob Full Screen")
                        .setAction("Revmob Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());
                Bundle params = new Bundle();
                params.putString("Revmob Full Screen", sp.getString("account", "notset"));
                params.putString("Revmob Full Clicked ", sp.getString("account", "notset"));
                mFirebaseAnalytics.logEvent("Revmob Full Screen Ad", params);
            }

            @Override
            public void onRevMobAdDisplayed() {
                Log.e("RevMob", "Fullscreen displayed.");
            }
        });
        startAd();

        MobileCore.init(this, "800OHIQUHSHMFMZ1JQ6PT91JH888A", MobileCore.LOG_TYPE.DEBUG,
                MobileCore.AD_UNITS.INTERSTITIAL);

        home.setText("Home");

        v = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);


        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        Account account = getAccount(AccountManager.get(getApplicationContext()));

        if (account == null) {
            accountName = "notSet";
            editor.putString("account", accountName);
            editor.commit();
        } else {
            accountName = account.name;
            editor.putString("account", accountName);
            editor.commit();
        }

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mNavigationView.setItemIconTintList(null);
        lv = (ListView) findViewById(R.id.lv);
        mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        menu = mNavigationView.getMenu();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.year) {
                    //startActivity(new Intent(MainActivity.this, Genre.class).putExtra("genre", "Years").putExtra("playLink", "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&sortyear=DESC"));
                    sort(s, e, "year");
                    mDrawerLayout.closeDrawers();
                    //getSupportActionBar().setTitle("year");
                    home.setText("year");
                }
                if (menuItem.getItemId() == R.id.likeUs) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Free-Movies-Online-244488529234196"));
                    startActivity(browserIntent);
                }
                if (menuItem.getItemId() == R.id.lastPlayed) {
                    if (!sp.getString("lastPlayedName", "null").equals("null")) {
                        startActivity(new Intent(MainActivity.this, Player.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("srt", sp.getString("lastPlayedSrt", "null"))
                                .putExtra("name", sp.getString("lastPlayedName", "null"))
                                .putExtra("playLink", sp.getString("lastPlayedUrl", "null")));

                    } else {
                        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) MainActivity.this
                                .findViewById(android.R.id.content)).getChildAt(0);
                        Snackbar snackbar = Snackbar
                                .make(viewGroup, "No Movie Played Recently", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }
                }
                if (menuItem.getItemId() == R.id.tvshow) {
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(MainActivity.this, TVShow.class));
                }
                if (menuItem.getItemId() == R.id.New) {
                    //fireIntent("name");
                    sort(s, e, "name");
                    mDrawerLayout.closeDrawers();
                    //getSupportActionBar().setTitle("name");
                    home.setText("name");
                }
                if (menuItem.getItemId() == R.id.about) {
                    startActivity(new Intent(MainActivity.this, About.class));
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.dm) {
                    startActivity(new Intent(MainActivity.this, DirectContact.class));
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.trends) {
                    startActivity(new Intent(MainActivity.this, Trends.class));
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.rate) {
                    String appPackageName = getPackageName();
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
                    marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(marketIntent);
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.size) {
                    //fireIntent("size");
                    sort(s, e, "size");
                    mDrawerLayout.closeDrawers();
                    //getSupportActionBar().setTitle("size");
                    home.setText("size");
                }
                if (menuItem.getItemId() == R.id.quality) {
                    //fireIntent("quality");
                    sort(s, e, "quality");
                    mDrawerLayout.closeDrawers();
                    //getSupportActionBar().setTitle("quality");
                    home.setText("quality");
                }
                if (menuItem.getItemId() == R.id.rating) {
                    //fireIntent("rating");
                    sort(s, e, "rating");
                    mDrawerLayout.closeDrawers();
                    //getSupportActionBar().setTitle("rating");
                    home.setText("rating");
                }
                if (menuItem.getItemId() == R.id.Action) {
                    fireIntent("Action");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Adventure) {
                    fireIntent("Adventure");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Animation) {
                    fireIntent("Animation");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Biography) {
                    fireIntent("Biography");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Comedy) {
                    fireIntent("Comedy");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Crime) {
                    fireIntent("Crime");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Drama) {
                    fireIntent("Drama");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Family) {
                    fireIntent("Family");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Fantasy) {
                    fireIntent("Fantasy");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.History) {
                    fireIntent("History");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Horror) {
                    fireIntent("Horror");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Mystery) {
                    fireIntent("Mystery");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.SciFi) {
                    fireIntent("Sci_Fi");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Sport) {
                    fireIntent("Sport");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Thriller) {
                    fireIntent("Thriller");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.Western) {
                    fireIntent("Western");
                    mDrawerLayout.closeDrawers();
                }
                if (menuItem.getItemId() == R.id.War) {
                    fireIntent("War");
                    mDrawerLayout.closeDrawers();
                }
                return false;
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        hit(s, e);
                                    }
                                }
        );

    }

    public void fireIntent(String genre) {
        if (genre.equals("name")) {
            startActivity(new Intent(MainActivity.this, Genre.class).putExtra("genre", "By Name").putExtra("playLink", "http://android.teknobilim.org/Movie/name.php?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&sortname=ASC"));
        } else if (genre.equals("rating")) {
            startActivity(new Intent(MainActivity.this, Genre.class).putExtra("genre", "By Rating").putExtra("playLink", "http://android.teknobilim.org/Movie/rating.php?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&sortrating=DESC"));
        } else if (genre.equals("size")) {
            startActivity(new Intent(MainActivity.this, Genre.class).putExtra("genre", "By Size").putExtra("playLink", "http://android.teknobilim.org/Movie/size.php?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&sortsize=ASC"));
        } else if (genre.equals("quality")) {
            startActivity(new Intent(MainActivity.this, Genre.class).putExtra("genre", "By Quality").putExtra("playLink", "http://android.teknobilim.org/Movie/quality.php?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&sortquality=DESC"));
        } else {
            startActivity(new Intent(MainActivity.this, Genre.class).putExtra("search", genre).putExtra("genre", genre).putExtra("playLink", "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0&f=29&genresearch=" + genre + "&sort=DESC"));
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        // Inflate the menu; this adds items to the action bar if it is present.
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        actionbarMenu = menu.findItem(R.id.action_settings);
        actionbarMenu.setVisible(false);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //final int searchBarId = searchView.getContext().getResources().getIdentifier("android:id/search_bar", null, null);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 2000);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);

        MenuItem mi = menu.findItem(R.id.action_search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        if (onRefresh == null) {
                            hit(s, e);
                        } else {
                            hit(s, e);
                        }
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        hit(0, totalNumberOfMovie);
                        return true; // Return true to expand action view
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Search(newText);
                return true;
            }
        });


        return true;
    }

    private void Search(String newText) {
        ArrayList id, name, size, rate, year, resume, genre, srt, quality, download, image, descp, view, downloaded;
        id = new ArrayList();
        name = new ArrayList();
        size = new ArrayList();
        rate = new ArrayList();
        year = new ArrayList();
        resume = new ArrayList();
        genre = new ArrayList();
        srt = new ArrayList();
        quality = new ArrayList();
        download = new ArrayList();
        view = new ArrayList();
        downloaded = new ArrayList();
        image = new ArrayList();
        descp = new ArrayList();
        if (this.name != null && this.name.size() != 0) {
            for (int t = 0; t < this.name.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.name.get(t)).find()) {
                    name.add(this.name.get(t));
                    size.add(this.size.get(t));
                    rate.add(this.rate.get(t));
                    year.add(this.year.get(t));
                    resume.add(this.resume.get(t));
                    genre.add(this.genre.get(t));
                    srt.add(this.srt.get(t));
                    quality.add(this.quality.get(t));
                    download.add(this.download.get(t));
                    image.add(this.image.get(t));
                    descp.add(this.descp.get(t));
                    view.add(this.views.get(t));
                    id.add(this.id.get(t));
                    downloaded.add(this.downloaded.get(t));
                }
            }
        }
        LVAdapter adapter = new LVAdapter(MainActivity.this, id, name, size, rate, year, resume, genre, srt, download, quality, image, descp, view, downloaded);
        //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (name != null && name.size() != 0) {
                long total = sp.getLong("total", -1);
                editor.putLong("total", totalNumberOfMovie);
                editor.commit();
                //total = name.size() - total;
                Intent i = new Intent(MainActivity.this, NewlyArrived.class);
                String link = "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=0" + "&f=" + totalNumberOfMovie + "&id";
                i.putExtra("playLink", link);
                startActivity(i);
                actionbarMenu.setVisible(false);
            }

            return true;
        }
        if (id == R.id.home) {
            if (name != null && name.size() != 0) {
                hit(s, e);
                //getSupportActionBar().setTitle("Home");
                home.setText("Home");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if (onRefresh == null) {
            if (home.getText().toString().equals("Home")) {
                hit(s, e);
            } else {
                sort(s, e, home.getText().toString());
            }

        } else {
            hit(s, e);
        }

    }

    void getNotice() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/notice.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jo = new JSONObject(response);
                            JSONArray notice = jo.getJSONArray("Notice");
                            JSONObject no = notice.getJSONObject(0);
                            //noticeText.setText(no.getString("notice") + ": ");
                            noticeText.setVisibility(View.GONE);
                            noticeMes.setText(no.getString("desc"));
                        } catch (JSONException e1) {
                            Toast.makeText(MainActivity.this, e1.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private class PullMovieInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            id = new ArrayList();
            name = new ArrayList();
            size = new ArrayList();
            rate = new ArrayList();
            year = new ArrayList();
            resume = new ArrayList();
            genre = new ArrayList();
            srt = new ArrayList();
            quality = new ArrayList();
            download = new ArrayList();
            image = new ArrayList();
            descp = new ArrayList();
            views = new ArrayList();
            downloaded = new ArrayList();
            try {
                JSONObject jsonObject1 = new JSONObject(params[0]);
                JSONArray jArr = jsonObject1.getJSONArray("FullMovie");
                JSONArray totalItems = jsonObject1.getJSONArray("Total");
                JSONObject total = totalItems.getJSONObject(0);
                totalNumberOfMovie = total.getInt("total");
                for (int t = 0; t < jArr.length(); t++) {
                    JSONObject jsonObject = jArr.getJSONObject(t);
                    id.add(jsonObject.get("id"));
                    name.add(jsonObject.get("name"));
                    size.add(jsonObject.get("size"));
                    rate.add(jsonObject.get("rating"));
                    year.add(jsonObject.get("year"));
                    resume.add(jsonObject.get("resume_capability"));
                    genre.add(jsonObject.get("genre"));
                    srt.add(jsonObject.get("srt"));
                    quality.add(jsonObject.get("quality"));
                    download.add(jsonObject.get("download"));
                    downloaded.add(jsonObject.get("downloaded"));
                    views.add(jsonObject.get("view"));
                    image.add(jsonObject.get("thumbnail"));
                    descp.add(jsonObject.get("description"));
                }

            } catch (JSONException e) {
                Log.e("error check", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LVAdapter adapter = new LVAdapter(MainActivity.this, id, name, size, rate, year, resume, genre, srt, download, quality, image, descp, views, downloaded);
            //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
            lv.setAdapter(adapter);
            int button = totalNumberOfMovie / 30;
            if (totalNumberOfMovie % 30 != 0) {
                button++;
            }
            button = button - mLinearScroll.getChildCount();
            setReadyPagination(button);

            if (sp.getLong("total", -1) < totalNumberOfMovie) {
                //MenuItem New = actionbarMenu.findItem(R.id.action_settings);
                actionbarMenu.setVisible(true);
            } else {
                actionbarMenu.setVisible(false);
            }

            swipeRefreshLayout.setRefreshing(false);
            //itemNumbers(genre);

            //ad.show();
        }

        private void setReadyPagination(int button) {
            if (name != null && name.size() != 0) {
                //int size = (totalNumberOfMovie / 30) + 1;

                for (int j = 0; j < button; j++) {
                    final int k;
                    k = j;
                    final Button btnPage = new Button(MainActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 0);
                    btnPage.setTextColor(Color.WHITE);
                    btnPage.setTextSize(13.0f);
                    btnPage.setId(j);
                    btnPage.setText(String.valueOf(mLinearScroll.getChildCount() + 1));
                    btnPage.setBackgroundResource(R.drawable.button_unfocused);
                    if (j == 0) {
                        previousClicked = btnPage;
                        btnPage.setBackgroundResource(R.drawable.button_focus);
                        previousClicked.setBackgroundResource(R.drawable.button_focus);
                        btnPage.setTextColor(Color.parseColor("#000000"));
                    }
                    mLinearScroll.addView(btnPage, lp);


                    btnPage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            previousClicked.setBackgroundResource(R.drawable.pagebuttontranssss);
                            previousClicked.setTextColor(Color.parseColor("#ffffff"));

                            btnPage.setBackgroundResource(R.drawable.pagebuttontrans);
                            btnPage.setTextColor(Color.parseColor("#000000"));
                            TransitionDrawable transition = (TransitionDrawable) btnPage.getBackground();
                            transition.startTransition(250);
                            TransitionDrawable transition2 = (TransitionDrawable) previousClicked.getBackground();
                            transition2.startTransition(250);
                            previousClicked = btnPage;
                            s = (30 * btnPage.getId());
                            e = ((30 * (btnPage.getId() + 1)) - 1);
                            Log.e("adfdsafadsfasdf", e + "");
                            //onRefresh = "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=" + (30 * Integer.parseInt("" + btnPage.getId())) + "&f=" + ((30 * Integer.parseInt("" + (btnPage.getId() + 1))) - 1) + "&id";
                            //new PullMovieInfo().execute(onRefresh);
                            if (home.getText().toString().equals("Home")) {
                                hit(s, e);
                            } else {
                                sort(s, e, home.getText().toString());
                            }

                        }
                    });
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getNotice();
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    public String printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "triumphit.free.movie.online",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                //Log.e("key", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                return Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return "null";
    }

    void hit(final int s, final int e) {
        Log.e("page", s + " " + e);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, test,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (snackbar != null) {
                                    snackbar.dismiss();
                                }
                            }
                        });
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", "usertest");
                params.put("email", "asdf1234");
                params.put("password", "asdf1234");
                String key = printHashKey(MainActivity.this);
                String creds = String.format("%s", key);
                String auth = Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                params.put("keyhash", loadCodec());
                params.put("s", "" + s);
                params.put("e", "" + e);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    void sort(final int s, final int e, final String ele) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, sortLink,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Could not connect to the internet", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", "usertest");
                params.put("email", "asdf1234");
                params.put("password", "asdf1234");
                String key = printHashKey(MainActivity.this);
                String creds = String.format("%s", key);
                String auth = Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                params.put("keyhash", auth);
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("sort", ele);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    protected void onDestroy() {
        //adView.destroy();
        super.onDestroy();
    }
}
