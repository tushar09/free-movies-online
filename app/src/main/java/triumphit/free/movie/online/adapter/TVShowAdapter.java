package triumphit.free.movie.online.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

import triumphit.free.movie.online.R;
import triumphit.free.movie.online.Seasons;

/**
 * Created by Tushar on 5/15/2016.
 */
public class TVShowAdapter extends BaseAdapter {

    ArrayList name, totalSeason, rating, year, genre, image;
    LayoutInflater inflater;
    Context context;
    static Animation animation;
    private int lastPosition = -1;

    public  TVShowAdapter(Context context, ArrayList name, ArrayList totalSeason, ArrayList rating, ArrayList year, ArrayList genre, ArrayList image){
        this.name = name;
        this.totalSeason = totalSeason;
        this.rating = rating;
        this.year = year;
        this.genre = genre;
        this.context = context;
        this.image = image;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }

        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.tvshowrow, null);
            holder = new Holder();
            holder.cv = (CardView) convertView.findViewById(R.id.view);
            holder.name = (TextView) convertView.findViewById(R.id.textView3);
            holder.totalSeason = (TextView) convertView.findViewById(R.id.textView2);
            holder.rating = (TextView) convertView.findViewById(R.id.textView4);
            holder.year = (TextView) convertView.findViewById(R.id.textView5);
            holder.genre = (TextView) convertView.findViewById(R.id.textView7);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.name.setText("" + name.get(position));
        holder.totalSeason.setText("" + totalSeason.get(position));
        holder.rating.setText("" + rating.get(position));
        holder.year.setText("" + year.get(position));
        holder.genre.setText("" + genre.get(position));
        Picasso.with(context).load("" + image.get(position)).fit().centerCrop().placeholder(R.drawable.placeholder).into(holder.thumbnail);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seasons = ("" + totalSeason.get(position)).replaceAll("Seasons: ", "");
                Intent i = new Intent(context, Seasons.class);
                i.putExtra("name", "" + name.get(position));
                i.putExtra("seasons", seasons);
                context.startActivity(i);
            }
        });

        return convertView;
    }

    public class Holder {
        CardView cv;
        TextView name, totalSeason, rating, year, genre;
        ImageView thumbnail;
    }
}
