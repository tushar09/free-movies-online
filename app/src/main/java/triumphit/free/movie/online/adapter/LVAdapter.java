package triumphit.free.movie.online.adapter;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import triumphit.free.movie.online.AnalyticsTrackers;
import triumphit.free.movie.online.DownloadManager.DownloadService;
import triumphit.free.movie.online.Episodes;
import triumphit.free.movie.online.Player;
import triumphit.free.movie.online.R;
import triumphit.free.movie.online.ServiceHandler;

import static android.view.View.VISIBLE;

/**
 * Created by Tushar on 3/11/2016.
 */
public class LVAdapter extends BaseAdapter {

    private final FirebaseAnalytics mFirebaseAnalytics;
    DownloadService downloadService;
    boolean isBound = false;

    static Animation animation;
    private final SharedPreferences sp;
    private final SharedPreferences.Editor editor;
    private final AnalyticsTrackers at;
    private final Tracker t;
    ArrayList id, name, size, rate, year, resume, genre, srt, download, quality, image, descp, view, downloaded;
    Context context;
    private int lastPosition = -1;
    private LayoutInflater inflater;

    ProgressDialog pdialog;

    ArrayList tracker;
    private Snackbar snackbar;

    public native String loadCodec();
    public native String loadCodecDir();

    public LVAdapter(Context context, ArrayList id, ArrayList name, ArrayList size, ArrayList rate, ArrayList year, ArrayList resume, ArrayList genre, ArrayList srt, ArrayList download, ArrayList quality, ArrayList image, ArrayList descp, ArrayList view, ArrayList downloaded) {
        this.context = context;
        this.id = id;
        this.name = name;
        this.size = size;
        this.rate = rate;
        this.year = year;
        this.resume = resume;
        this.genre = genre;
        this.srt = srt;
        this.download = download;
        this.view = view;
        this.downloaded = downloaded;
        this.quality = quality;
        this.image = image;
        this.descp = descp;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tracker = new ArrayList();

        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        AnalyticsTrackers.initialize(context);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }

        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row, null);
            holder = new Holder();
            holder.cv = (CardView) convertView.findViewById(R.id.view);
            holder.name = (TextView) convertView.findViewById(R.id.textView3);
            holder.size = (TextView) convertView.findViewById(R.id.textView2);
            holder.rate = (TextView) convertView.findViewById(R.id.textView4);
            holder.year = (TextView) convertView.findViewById(R.id.textView5);
            holder.resume = (TextView) convertView.findViewById(R.id.textView9);
            holder.genre = (TextView) convertView.findViewById(R.id.textView7);
            holder.srt = (TextView) convertView.findViewById(R.id.textView);
            holder.quality = (TextView) convertView.findViewById(R.id.textView10);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.imageView);
            holder.download = (ImageButton) convertView.findViewById(R.id.imageButton6);
            holder.description = (TextView) convertView.findViewById(R.id.textView11);
            holder.updown = (ImageButton) convertView.findViewById(R.id.imageButton7);
            holder.details = (RelativeLayout) convertView.findViewById(R.id.details);
            holder.view = (TextView) convertView.findViewById(R.id.textView21);
            holder.downloaded = (TextView) convertView.findViewById(R.id.textView22);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.name.setText("" + name.get(position));
        holder.size.setText("" + size.get(position));
        holder.rate.setText("" + rate.get(position));
        holder.year.setText("" + year.get(position));
        holder.resume.setText("" + resume.get(position));
        holder.description.setText("" + descp.get(position));
        holder.view.setText("" + view.get(position));
        holder.downloaded.setText("" + downloaded.get(position));
        if (resume.get(position).equals("no")) {
            holder.resume.setTextColor(Color.RED);
        } else {
            holder.resume.setTextColor(Color.BLACK);
        }
        holder.genre.setText("" + genre.get(position));
        if (!("" + srt.get(position)).equals("no")) {
            holder.srt.setText("yes");
            holder.srt.setTextColor(Color.parseColor("#00BD68"));
        } else {
            holder.srt.setText("no");
            holder.srt.setTextColor(Color.parseColor("#000000"));
        }
        holder.quality.setText("" + quality.get(position));
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCodec("" + download.get(position), position, "d");
                Log.e("d link", "" + download.get(position));
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("FREE MOVIES ONLINE")
                        .setMessage("Do you want to download " + name.get(position) + ", size: " + "" + size.get(position))
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
//                                Log.e("binder Test", downloadService.getString());
//                                Intent i = new Intent(context, DownloadService.class);
//                                i.putExtra("title", "" + name.get(position));
//                                context.startService(i);

                                DownloadManager.Request r = new DownloadManager.Request(Uri.parse("" + download.get(position)));
                                Log.e("download Clicked", download.get(position) + "");
                                String downloadPath = "/Free Movies Online";
                                File fileDir = new File(downloadPath);

                                if (fileDir.isDirectory()) {
                                    String extention = "";
                                    if (("" + download.get(position)).endsWith(".mkv")) {
                                        extention = ".mkv";
                                    } else if (("" + download.get(position)).endsWith(".MKV")) {
                                        extention = ".MKV";
                                    } else if (("" + download.get(position)).endsWith(".MP4")) {
                                        extention = ".MP4";
                                    } else if (("" + download.get(position)).endsWith(".mp4")) {
                                        extention = ".mp4";
                                    } else if (("" + download.get(position)).endsWith(".AVI")) {
                                        extention = ".AVI";
                                    } else if (("" + download.get(position)).endsWith(".avi")) {
                                        extention = ".avi";
                                        Log.e("download Clicked", "avi");
                                    } else if (("" + download.get(position)).endsWith(".3gp")) {
                                        extention = ".3gp";
                                    } else if (("" + download.get(position)).endsWith(".3GP")) {
                                        extention = ".3GP";
                                    }
                                    r.setDestinationInExternalPublicDir(Environment.DIRECTORY_MOVIES, "" + name.get(position) + extention);
                                } else {
                                    fileDir.mkdirs();
                                    String extention = "";
                                    if (("" + download.get(position)).endsWith(".mkv")) {
                                        extention = ".mkv";
                                    } else if (("" + download.get(position)).endsWith(".MKV")) {
                                        extention = ".MKV";
                                    } else if (("" + download.get(position)).endsWith(".MP4")) {
                                        extention = ".MP4";
                                    } else if (("" + download.get(position)).endsWith(".mp4")) {
                                        extention = ".mp4";
                                    } else if (("" + download.get(position)).endsWith(".AVI")) {
                                        extention = ".AVI";
                                    } else if (("" + download.get(position)).endsWith(".avi")) {
                                        extention = ".avi";
                                        Log.e("download Clicked", "avi else");
                                    } else if (("" + download.get(position)).endsWith(".3gp")) {
                                        extention = ".3gp";
                                    } else if (("" + download.get(position)).endsWith(".3GP")) {
                                        extention = ".3GP";
                                    }
                                    r.setDestinationInExternalPublicDir(Environment.DIRECTORY_MOVIES, "" + name.get(position) + extention);
                                }
                                // This put the download in the same Download dir the browser uses
                                //r.setDestinationInExternalPublicDir(dir, "" + name.get(position));

                                // When downloading music and videos they will be listed in the player
                                // (Seems to be available since Honeycomb only)
                                r.allowScanningByMediaScanner();

                                // Notify user when download is completed
                                // (Seems to be available since Honeycomb only)
                                r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                // Start download
                                DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                dm.enqueue(r);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.main_x);
                final AlertDialog a = builder.create();
                a.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    }
                });
                a.show();
                //holder.description.setVisibility(VISIBLE);
            }
        });

        holder.updown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("d link", "" + download.get(position));
                //holder.description.setVisibility(VISIBLE);
                if (holder.description.getVisibility() == VISIBLE) {
                    collapse(holder.description);
                    collapse(holder.details);
                    tracker.remove(descp.get(position));
                } else {
                    expand(holder.description);
                    expand(holder.details);
                    tracker.add(descp.get(position));
                    holder.trated = (TextView) holder.details.findViewById(R.id.textView31);
                    holder.treleshed = (TextView) holder.details.findViewById(R.id.textView32);
                    holder.truntime = (TextView) holder.details.findViewById(R.id.textView33);
                    holder.tdirector = (TextView) holder.details.findViewById(R.id.textView34);
                    holder.twriter = (TextView) holder.details.findViewById(R.id.textView35);
                    holder.tplot = (TextView) holder.details.findViewById(R.id.textView37);
                    holder.tlanguage = (TextView) holder.details.findViewById(R.id.textView38);
                    holder.tcountry = (TextView) holder.details.findViewById(R.id.textView39);
                    holder.taward = (TextView) holder.details.findViewById(R.id.textView40);
                    holder.timdbrating = (TextView) holder.details.findViewById(R.id.textView41);
                    holder.timdbvote = (TextView) holder.details.findViewById(R.id.textView42);
                    holder.act1 = (ImageView) holder.details.findViewById(R.id.imageView6);
                    holder.act2 = (ImageView) holder.details.findViewById(R.id.imageView7);
                    holder.act3 = (ImageView) holder.details.findViewById(R.id.imageView8);
                    holder.act4 = (ImageView) holder.details.findViewById(R.id.imageView9);
                    holder.actors = (TextView) holder.details.findViewById(R.id.textView43);


                    //String link = "http://www.omdbapi.com/?t=deadpool&y=&plot=full&r=json";
                    String link = "http://www.omdbapi.com/?t=" + name.get(position) + "&y=" + year.get(position) + "&plot=full&r=json";
//                    String ini = "http://www.omdbapi.com/?t=";
//                    String n = "" + name.get(position);
//                    n = n.replaceAll(" ", "+");
//                    String y = "&y=" + year.get(position);
//                    String rest = "&plot=full&r=json";
                    link = link.replaceAll(" ", "+");

                    AsyncTask at = new AsyncTask() {

                        String trated, treleshed, truntime, tdirector, twriter, tplot, tlanguage, tcountry, taward, timdbrating, timdbvote, actor;
                        String act1, act2, act3, act4;
                        String act1Url, act2Url, act3Url, act4Url;

                        @Override
                        protected String doInBackground(Object[] params) {
                            ServiceHandler sh = new ServiceHandler(context);
                            String jsonStr = sh.makeServiceCall("" + params[0], ServiceHandler.GET);

                            if (jsonStr != null) {
                                try {
                                    JSONObject jsonObject1 = new JSONObject(jsonStr);

                                    //for (int t = 0; t < jArr.length(); t++) {
                                    //Log.e("asdf", "sdfasdf");
                                    //Log.e("err", "" + jsonObject.get("name"));
                                    actor = jsonObject1.getString("Actors");
                                    Log.e("actors 1", actor);

                                    String[] act = actor.split(", ");
                                    for (int t = 0; t < act.length; t++) {
                                        if (t == 0) {
                                            act1 = act[t];
                                            Log.e("act length", "" + act.length);
                                            act1 = act1.replaceAll(" ", "_");
                                            Log.e("act1", act1);
                                            String actorImage = "https://en.wikipedia.org/w/api.php?action=query&titles=" + act1 + "&prop=pageimages&format=json&pithumbsize=500";
                                            String json = sh.makeServiceCall(actorImage, ServiceHandler.GET);
                                            JSONObject jsonActorImage = new JSONObject(json);
                                            JSONObject query = jsonActorImage.getJSONObject("query");
                                            JSONObject pages = query.getJSONObject("pages");
                                            JSONObject finalObj = pages.getJSONObject(pages.names().getString(0));
                                            if (finalObj.has("thumbnail")) {
                                                JSONObject thum = finalObj.getJSONObject("thumbnail");
                                                act1Url = thum.getString("source");
                                                Log.e("Image ase", "Key = " + thum.getString("source"));
                                            } else {
                                                Log.e("Image Nai", "Image Nai");
                                            }
                                        } else if (t == 1) {
                                            act2 = act[t];
                                            Log.e("act length", "" + act.length);
                                            act2 = act2.replaceAll(" ", "_");
                                            Log.e("act2", act2);
                                            String actorImage = "https://en.wikipedia.org/w/api.php?action=query&titles=" + act2 + "&prop=pageimages&format=json&pithumbsize=500";
                                            String json = sh.makeServiceCall(actorImage, ServiceHandler.GET);
                                            JSONObject jsonActorImage = new JSONObject(json);
                                            JSONObject query = jsonActorImage.getJSONObject("query");
                                            JSONObject pages = query.getJSONObject("pages");
                                            JSONObject finalObj = pages.getJSONObject(pages.names().getString(0));
                                            if (finalObj.has("thumbnail")) {
                                                JSONObject thum = finalObj.getJSONObject("thumbnail");
                                                act2Url = thum.getString("source");
                                                Log.e("Image ase", "Key = " + thum.getString("source"));
                                            } else {
                                                Log.e("Image Nai", "Image Nai");
                                            }
                                        } else if (t == 2) {
                                            act3 = act[t];
                                            Log.e("act length", "" + act.length);
                                            act3 = act3.replaceAll(" ", "_");
                                            Log.e("act3", act3);
                                            String actorImage = "https://en.wikipedia.org/w/api.php?action=query&titles=" + act3 + "&prop=pageimages&format=json&pithumbsize=500";
                                            String json = sh.makeServiceCall(actorImage, ServiceHandler.GET);
                                            JSONObject jsonActorImage = new JSONObject(json);
                                            JSONObject query = jsonActorImage.getJSONObject("query");
                                            JSONObject pages = query.getJSONObject("pages");
                                            JSONObject finalObj = pages.getJSONObject(pages.names().getString(0));
                                            if (finalObj.has("thumbnail")) {
                                                JSONObject thum = finalObj.getJSONObject("thumbnail");
                                                act3Url = thum.getString("source");
                                                Log.e("Image ase", "Key = " + thum.getString("source"));
                                            } else {
                                                Log.e("Image Nai", "Image Nai");
                                            }

                                            //Picasso.with(context).load(thum.getString("source")).memoryPolicy(MemoryPolicy.NO_CACHE).fit().centerCrop().into(holder.act1);


                                        } else if (t == 3) {
                                            act4 = act[t];
                                            Log.e("act length", "" + act.length);
                                            act4 = act4.replaceAll(" ", "_");
                                            Log.e("act4", act4);
                                            String actorImage = "https://en.wikipedia.org/w/api.php?action=query&titles=" + act4 + "&prop=pageimages&format=json&pithumbsize=500";
                                            String json = sh.makeServiceCall(actorImage, ServiceHandler.GET);
                                            JSONObject jsonActorImage = new JSONObject(json);
                                            JSONObject query = jsonActorImage.getJSONObject("query");
                                            JSONObject pages = query.getJSONObject("pages");
                                            JSONObject finalObj = pages.getJSONObject(pages.names().getString(0));
                                            if (finalObj.has("thumbnail")) {
                                                JSONObject thum = finalObj.getJSONObject("thumbnail");
                                                act4Url = thum.getString("source");
                                                Log.e("Image ase", "Key = " + thum.getString("source"));
                                            } else {
                                                Log.e("Image Nai", "Image Nai");
                                            }
                                        }
                                    }

                                    trated = jsonObject1.getString("Rated");
                                    treleshed = jsonObject1.getString("Released");
                                    truntime = jsonObject1.getString("Runtime");
                                    tdirector = jsonObject1.getString("Director");
                                    twriter = jsonObject1.getString("Writer");
                                    tplot = jsonObject1.getString("Plot");
                                    tlanguage = jsonObject1.getString("Language");
                                    tcountry = jsonObject1.getString("Country");
                                    taward = jsonObject1.getString("Awards");
                                    timdbrating = jsonObject1.getString("imdbRating");
                                    timdbvote = jsonObject1.getString("imdbVotes");

                                } catch (JSONException e) {
                                    Log.e("int search result", "" + e.toString());
                                }
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(0);
                            holder.trated.setText(Html.fromHtml("<font color=#E91E63>Rated: </font> <font color=#000000>" + trated + "</font>"));
                            holder.treleshed.setText(Html.fromHtml("<font color=#E91E63>Released: </font> <font color=#000000>" + treleshed + "</font>"));
                            holder.truntime.setText(Html.fromHtml("<font color=#E91E63>Runtime: </font> <font color=#000000>" + truntime + "</font>"));
                            holder.tdirector.setText(Html.fromHtml("<font color=#E91E63>Director: </font> <font color=#000000>" + tdirector + "</font>"));
                            holder.twriter.setText(Html.fromHtml("<font color=#E91E63>Writer: </font> <font color=#000000>" + twriter + "</font>"));
                            holder.tplot.setText(Html.fromHtml("<font color=#E91E63>Plot: </font> <font color=#000000>" + tplot + "</font>"));
                            holder.tlanguage.setText(Html.fromHtml("<font color=#E91E63>Language: </font> <font color=#000000>" + tlanguage + "</font>"));
                            holder.tcountry.setText(Html.fromHtml("<font color=#E91E63>Country:</font> <font color=#000000>" + tcountry + "</font>"));
                            holder.taward.setText(Html.fromHtml("<font color=#E91E63>Awards: </font> <font color=#000000>" + taward + "</font>"));
                            holder.timdbrating.setText(Html.fromHtml("<font color=#E91E63>imdbrating: </font> <font color=#000000>" + timdbrating + "</font>"));
                            holder.timdbvote.setText(Html.fromHtml("<font color=#E91E63>imdbvote: </font> <font color=#000000>" + timdbvote + "</font>"));
                            holder.actors.setText(Html.fromHtml("<font color=#E91E63>Actors: </font> <font color=#000000>" + actor + "</font>"));
                            Picasso.with(context).load(act1Url).fit().centerCrop().placeholder(R.drawable.placeholderavt).into(holder.act1);
                            Picasso.with(context).load(act2Url).fit().centerCrop().placeholder(R.drawable.placeholderavt).into(holder.act2);
                            Picasso.with(context).load(act3Url).fit().centerCrop().placeholder(R.drawable.placeholderavt).into(holder.act3);
                            Picasso.with(context).load(act4Url).fit().centerCrop().placeholder(R.drawable.placeholderavt).into(holder.act4);
                        }

                    };
                    at.execute(link);

                }
            }
        });

        Picasso.with(context).load("" + image.get(position)).fit().centerCrop().placeholder(R.drawable.placeholder).into(holder.thumbnail);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;
        if (tracker.contains(descp.get(position))) {
            holder.description.setVisibility(VISIBLE);
            holder.details.setVisibility(VISIBLE);
        } else {
            holder.description.setVisibility(View.GONE);
            holder.details.setVisibility(View.GONE);
        }

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCodec("" + download.get(position), position, "v");

                context.startActivity(new Intent(context, Player.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("srt", "" + srt.get(position))
                        .putExtra("name", "" + name.get(position))
                        .putExtra("playLink", "" + download.get(position)));
                Log.e("srt", "" + srt.get(position));

                editor.putString("lastPlayedUrl", "" + download.get(position));
                editor.putString("lastPlayedSrt", "" + srt.get(position));
                editor.putString("lastPlayedName", "" + name.get(position));
                editor.commit();
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Movie Viewed")
                        .setAction("" + name.get(position))
                        .setLabel("Played " + name.get(position))
                        .build());
                Bundle params = new Bundle();
                params.putString("Movie Viewed", "" + name.get(position));
                mFirebaseAnalytics.logEvent("Movie Viewed", params);
                Log.e("srt url", "" + srt.get(position));



            }
        });

        holder.cv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                PopupMenu pm = new PopupMenu(context, v);
                MenuInflater mi = pm.getMenuInflater();
                mi.inflate(R.menu.menu_report, pm.getMenu());
                pm.show();

                pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.report) {
                            Log.e("before report", "" + name.get(position));
                            report("" + name.get(position), v);
                        }
                        return false;
                    }
                });

                Log.e("nativ", loadCodec() + " " + loadCodecDir());

                return false;
            }
        });

        return convertView;
    }

    private void setCodec(final String link, final int position, final String type) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/movieAuth/viewAndDownloaded.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", type);
                Log.e("type", type);
                params.put("id", id.get(position).toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

//    private void getDetails(String link) {
//        new PullMovieInfo().execute(link);
//    }

    void report(final String name, final View v) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/reportedMovie.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("before report", response);
                        Toast.makeText(context, response.replaceAll("\"", ""), Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("before report", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public class Holder {
        CardView cv;
        TextView name, size, rate, year, resume, genre, srt, quality, description, view, downloaded;
        TextView trated, treleshed, truntime, tdirector, twriter, tplot, tlanguage, tcountry, taward, timdbrating, timdbvote, actors;
        ImageView thumbnail, act1, act2, act3, act4;
        ImageButton download, updown;
        RelativeLayout details;
    }

}
