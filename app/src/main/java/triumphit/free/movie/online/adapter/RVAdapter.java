package triumphit.free.movie.online.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import triumphit.free.movie.online.R;

/**
 * Created by Tushar on 3/11/2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder>{

    ArrayList name, size, rate, year, resume, genre, srt, download, quality, image;
    Context context;
    private int lastPosition = -1;
    public RVAdapter(Context context, ArrayList name, ArrayList size, ArrayList rate, ArrayList year, ArrayList resume, ArrayList genre, ArrayList srt, ArrayList download, ArrayList quality, ArrayList image){
        this.context = context;
        this.name = name;
        this.size = size;
        this.rate = rate;
        this.year = year;
        this.resume = resume;
        this.genre = genre;
        this.srt = srt;
        this.download = download;
        this.quality = quality;
        this.image = image;
    }


    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
//        Animation animationFadeIn = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
//        v.startAnimation(animationFadeIn);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, final int position) {
        holder.name.setText("" + name.get(position));
        holder.size.setText("" + size.get(position));
        holder.rate.setText("" + rate.get(position));
        holder.year.setText("" + year.get(position));
        holder.resume.setText("" + resume.get(position));
        if(resume.get(position).equals("no")){
            holder.resume.setTextColor(Color.RED);
        }
        holder.genre.setText("" + genre.get(position));
        if(!("" + srt.get(position)).equals("no")){
            holder.srt.setText("yes");
            holder.srt.setTextColor(Color.parseColor("#00BD68"));
        }else{
            holder.srt.setText("no");
        }
        holder.quality.setText("" + quality.get(position));
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("d link", "" + download.get(position));
            }
        });
        Picasso.with(context).load("" + image.get(position)).fit().centerCrop().into(holder.thumbnail);
    }


    @Override
    public int getItemCount() {
        return name.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name, size, rate, year, resume, genre, srt, quality;
        ImageView thumbnail;
        ImageButton download;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.view);
            name = (TextView)itemView.findViewById(R.id.textView3);
            size = (TextView)itemView.findViewById(R.id.textView2);
            rate = (TextView)itemView.findViewById(R.id.textView4);
            year = (TextView)itemView.findViewById(R.id.textView5);
            resume = (TextView)itemView.findViewById(R.id.textView9);
            genre = (TextView)itemView.findViewById(R.id.textView7);
            srt = (TextView)itemView.findViewById(R.id.textView);
            quality = (TextView)itemView.findViewById(R.id.textView10);
            thumbnail = (ImageView) itemView.findViewById(R.id.imageView);
            download = (ImageButton) itemView.findViewById(R.id.imageButton6);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
