package triumphit.free.movie.online.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import triumphit.free.movie.online.Episodes;
import triumphit.free.movie.online.R;

/**
 * Created by Tushar on 5/16/2016.
 */
public class TVShowSeasonsAdapter extends BaseAdapter{

    String [] seasons;
    String TvShowName;
    LayoutInflater inflater;
    Context context;

    ProgressDialog pdialog;


    private ArrayList<String> download, title, released, episodes, imdbRating;

    public TVShowSeasonsAdapter(Context c, String [] seasons, String TvShowName){
        this.seasons = seasons;
        this.TvShowName = TvShowName;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = c;
    }

    @Override
    public int getCount() {
        return seasons.length;
    }

    @Override
    public Object getItem(int position) {
        return seasons[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = inflater.inflate(R.layout.tvshow_seasons_row, null);
            holder.seasonsNumber = (TextView) convertView.findViewById(R.id.textView50);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }
        holder.seasonsNumber.setText("Seasons: " + seasons[position]);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdialog = new ProgressDialog(context);
                pdialog.setCancelable(true);
                pdialog.setMessage("Setting Stream");
                pdialog.show();
                hit(seasons[position]);
            }
        });

        return convertView;
    }

    public void getEpisodInfo(String name, final String season) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://www.omdbapi.com/?t=" + name.replaceAll(" ", "%20") + "&Season=" + season,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response tv show", response);
                        pdialog.setMessage("Loading Episodes");

                        title = new ArrayList();
                        released = new ArrayList();
                        episodes = new ArrayList();
                        imdbRating = new ArrayList();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jArr = jsonObject.getJSONArray("Episodes");
                            Log.e("response", response);

                            for (int t = 0; t < jArr.length(); t++) {
                                JSONObject jObject = jArr.getJSONObject(t);
                                title.add("" +  jObject.get("Title"));
                                released.add("" +  jObject.get("Released"));
                                episodes.add("" +  jObject.get("Episode"));
                                imdbRating.add("" +  jObject.get("imdbRating"));
                            }
                            pdialog.dismiss();
                            Intent i = new Intent(context, Episodes.class);
                            i.putExtra("name", TvShowName);
                            i.putStringArrayListExtra("download", download);
                            i.putStringArrayListExtra("title", title);
                            i.putStringArrayListExtra("released", released);
                            i.putStringArrayListExtra("episodes", episodes);
                            i.putStringArrayListExtra("imdbRating", imdbRating);

                            context.startActivity(i);
                            //pdialog.dismiss();

                        } catch (JSONException e1) {
                            pdialog.setMessage("Could not connect to Internet.");
                        }
                        //new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    class Holder{
        TextView seasonsNumber;
    }

    private void hit(final String s) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/episode.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response tv show", response);
                        download = new ArrayList();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jArr = jsonObject.getJSONArray("FullEpisodes");
                            Log.e("response", response);

                            for (int t = 0; t < jArr.length(); t++) {
                                JSONObject jObject = jArr.getJSONObject(t);
                                download.add("" + jObject.get("episode1"));
                                download.add("" + jObject.get("episode2"));
                                download.add("" + jObject.get("episode3"));
                                download.add("" + jObject.get("episode4"));
                                download.add("" + jObject.get("episode5"));
                                download.add("" + jObject.get("episode6"));
                                download.add("" + jObject.get("episode7"));
                                download.add("" + jObject.get("episode8"));
                                download.add("" + jObject.get("episode9"));
                                download.add("" + jObject.get("episode10"));
                                download.add("" + jObject.get("episode11"));
                                download.add("" + jObject.get("episode12"));
                                download.add("" + jObject.get("episode13"));
                                download.add("" + jObject.get("episode14"));
                                download.add("" + jObject.get("episode15"));
                                download.add("" + jObject.get("episode16"));
                                download.add("" + jObject.get("episode17"));
                                download.add("" + jObject.get("episode18"));
                                download.add("" + jObject.get("episode19"));
                                download.add("" + jObject.get("episode20"));
                                download.add("" + jObject.get("episode21"));
                                download.add("" + jObject.get("episode22"));
                                download.add("" + jObject.get("episode23"));
                                download.add("" + jObject.get("episode24"));
                                download.add("" + jObject.get("episode25"));
                                download.add("" + jObject.get("episode26"));
                                download.add("" + jObject.get("episode27"));
                                download.add("" + jObject.get("episode28"));
                                download.add("" + jObject.get("episode29"));
                                download.add("" + jObject.get("episode30"));
                                download.add("" + jObject.get("episode31"));
                                download.add("" + jObject.get("episode32"));
                                download.add("" + jObject.get("episode33"));
                                download.add("" + jObject.get("episode34"));
                                download.add("" + jObject.get("episode35"));
                                download.add("" + jObject.get("episode36"));
                                download.add("" + jObject.get("episode37"));
                                download.add("" + jObject.get("episode38"));
                                download.add("" + jObject.get("episode39"));
                                download.add("" + jObject.get("episode40"));
                            }

                            pdialog.setMessage("Loading Episodes");
                            getEpisodInfo(TvShowName, s);
                            //pdialog.dismiss();
                            //context.startActivity(new Intent(context, Episodes.class));

                        } catch (JSONException e1) {
                            Log.e("response tv show", e1.toString());
                        }
                        //new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", TvShowName.toLowerCase());
                params.put("season", s);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

}
