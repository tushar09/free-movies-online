//
// Created by Tushar on 12/11/2016.
//
#include<jni.h>
#include<string.h>

jstring Java_triumphit_free_movie_online_MainActivity_loadCodec(JNIEnv* env, jobject obj){
    return(*env)->NewStringUTF(env, "9e3c3a232c379eca8c75e8a8c033270ba672d434");
}

jstring Java_triumphit_free_movie_online_adapter_LVAdapter_loadCodecDir(JNIEnv* env, jobject obj){
    return(*env)->NewStringUTF(env, "http://triumphit.tech/movieAuth/directLink.php");
}