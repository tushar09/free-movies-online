LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := mkvCodec
LOCAL_SRC_FILES := mkvCodec.c

include $(BUILD_SHARED_LIBRARY)